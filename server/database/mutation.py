import boto3
dynamodb = boto3.resource('dynamodb')
dynamodbClient = boto3.client('dynamodb')

from .query import getProjectByDepartment

tablename2 = 'VTI-Management-Setting-1'
tablename1 = 'VTI-Management-Database-1'


table2 = dynamodb.Table(tablename2)
table1 = dynamodb.Table(tablename1)


# SSO Settings

def editWhiteListDomain(domain):
    sk = 'whitelistdomain'
    pk = 'SETTING_PROPERTY'
    response = table2.update_item(
        Key={
            'pk': pk,
            'sk': sk
        },
        UpdateExpression='SET domainvalue = :val1',
        ExpressionAttributeValues={
            ':val1': domain
        }
    )
    return response


def editGoogleCredential(googleClientId, googleSecretId):
    sk = 'google'
    pk = 'SETTING_PROPERTY'
    response = table2.update_item(
        Key={
            'pk': pk,
            'sk': sk
        },
        UpdateExpression='SET googleClientId = :val1 , googleSecretId = :val2',
        ExpressionAttributeValues={
            ':val1': googleClientId,
            ':val2': googleSecretId
        }
    )
    return response


def deleteGoogleCredential():
    sk = 'google'
    pk = 'SETTING_PROPERTY'
    response = table2.delete_item(
        Key={
            'pk': pk,
            'sk': sk
        }
    )
    return response


def removeWhiteListDomain():
    sk = 'whitelistdomain'
    pk = 'SETTING_PROPERTY'
    response = table2.delete_item(
        Key={
            'pk': pk,
            'sk': sk
        }
    )
    return response

# DepartmentSetting()


def addNewDepartment(department):
    sk = 'DEPARTMENT'
    pk = 'department#'+department['id']
    datavalue = 'department#' + department['name']
    response = table1.put_item(
        Item={
            'pk': pk,
            'sk': sk,
            'datavalue': datavalue
        }
    )
    return response


def deleteDepartment(department):
    project = getProjectByDepartment(department['name'])
    print('check',len(project['Items']))
    if(len(project['Items'])> 0 ):
        return {
            'warning': 'This department is has relationship with {} projects'.format(len(project['Items']))

        }
    sk = 'DEPARTMENT'
    pk = department['id']
    response = table1.delete_item(
        Key={
            'pk': pk,
            'sk': sk
        }
    )
    return response


def updateDepartment(department):
    sk = 'DEPARTMENT'
    pk = department['id']
    oldName = department['oldName']
    name = 'department#'+department['name']
    response = table1.update_item(
        Key={
            'pk': pk,
            'sk': sk
        },
        UpdateExpression='SET datavalue = :val1',
        ExpressionAttributeValues={
            ':val1': name
        }
    )
    res = getProjectByDepartment(oldName)
    for e in res['Items']:
        table1.update_item(
            Key={
                'pk': e['pk'],
                'sk': e['sk']
            },
            UpdateExpression='SET datavalue = :val1',
            ExpressionAttributeValues={
                ':val1': 'project#'+ department['name']
            }
        )
    print(res)
    
    return response

# Project


def addNewProject(project):
    sk = 'PROJECT'
    pk = 'project#'+project['id']
    response = table1.put_item(
        Item={
            'pk': pk,
            'sk': sk,
            'datavalue': 'project#' + project['department'],
            'createdat': project['createdat'],
            'description': project['description'],
            'ownervalue': project['ownervalue'],
            'projectname': project['projectname'],
            'stages': []
        }
    )
    table1.put_item(
        Item={
            'pk': 'user#'+project['ownervalue'],
            'sk': 'user#'+project['id'],
            'datavalue':'user#owner'
        }
    )
    return response

def deleteProject(project):
    sk = 'PROJECT'
    pk = project['id']
    response = table1.delete_item(
        Key={
            'pk': pk,
            'sk': sk
        }
    )
    table1.delete_item(
        Key={
            'pk': 'user#' + project['ownervalue'],
            'sk': 'user#' + project['id'].split('#')[1]
        }
    )
    return response  

def editProject(project):
    response = table1.update_item(
        Key={
            'pk': project['pk'],
            'sk': project['sk']
        },
        UpdateExpression='''
        SET datavalue = :val1 , 
        createdat = :val2 , 
        description = :val3 ,
        ownervalue = :val4,
        projectname= :val5 
        ''',
        ExpressionAttributeValues={
            ':val1': project['datavalue'],
            ':val2': project['createdat'],
            ':val3': project['description'],
            ':val4': project['ownervalue'],
            ':val5': project['projectname']
        }
    )
    return response