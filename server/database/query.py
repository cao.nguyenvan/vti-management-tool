import boto3

from boto3.dynamodb.conditions import Key


tablename1 = 'VTI-Management-Database-1'
tablename2 = 'VTI-Management-Setting-1'

dynamodb = boto3.resource('dynamodb')
table1 = dynamodb.Table(tablename1)
table2 = dynamodb.Table(tablename2)


def getSSOSetting():
    pk = 'SETTING_PROPERTY'
    response = table2.query(KeyConditionExpression=Key('pk').eq(pk))
    print(response)
    return response['Items']




def getWhiteListDomain():

    sk = 'whitelistdomain'
    pk = 'SETTING_PROPERTY'
    response = table2.get_item(
        Key={
            'pk': pk,
            'sk': sk
        }
    )
    if ('Item' in response.keys()):
        print('check', response)
        return response['Item']
    return None


def getDepartments():
    response = table1.query(IndexName='gsi', KeyConditionExpression=Key('sk').eq('DEPARTMENT'))
    return response

def getAllProjects():
    response = table1.query(IndexName='gsi', KeyConditionExpression=Key('sk').eq('PROJECT'))
    return response

def getProjectByDepartment(department):
    response = table1.query(IndexName='gsi', KeyConditionExpression=Key('sk').eq('PROJECT') & Key('datavalue').eq('project#'+department))
    return response

def getProjectDetail(projectid):
    sk = 'PROJECT'
    pk = 'project#'+ projectid
    response = table1.get_item(
        Key={
            'pk': pk,
            'sk': sk
        }
    )
    return response

