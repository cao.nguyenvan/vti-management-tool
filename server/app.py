import logging
from flask import Flask, render_template ,jsonify,request
import os
from os import environ
import sys

sys.path.insert(0, './apis')
from ControlEc2 import ec2
from Cloudformation import cloudformation
from ControlRDS import rds
from SSOSetting import sso
from Users import user
from Vpc import vpc
from Department import department
from Project import project

# from jinja2 import Environment, FileSystemLoader

# THIS_DIR = os.path.dirname(os.path.abspath(__file__))
# jenv = Environment(loader=FileSystemLoader(THIS_DIR), trim_blocks=True)

app = Flask(__name__)


logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)



if(environ.get('FLASK_ENV')=='development'):
    path='/dev'
else:
    path=''

app.register_blueprint(ec2,url_prefix=path+'/apis/ec2')
app.register_blueprint(cloudformation,url_prefix=path+'/apis/cloudformation')
app.register_blueprint(rds,url_prefix=path+'/apis/rds')
app.register_blueprint(sso,url_prefix=path+'/apis/sso')
app.register_blueprint(user,url_prefix=path+'/apis/user')
app.register_blueprint(vpc,url_prefix=path+'/apis/vpc')
app.register_blueprint(department,url_prefix=path+'/apis/department')
app.register_blueprint(project, url_prefix=path+'/apis/project')

# @app.route(path+'/design',methods=['GET'])
# def get_design():
#     print('design')
#     return render_template('prod.html')

# @app.route(path+'/manage/<stackname>',methods=['GET'])
# def get_manage(stackname):
#     return render_template('prod.html')
@app.route(path+'/<path>',methods=['GET'])
def handler(path):
    if('/dev' in request.base_url):
        return render_template('dev.html')
        
    return render_template('prod.html')

@app.route(path+'/',methods=['GET'])
def handle_client():
    if('/dev' in request.base_url):
        return render_template('dev.html')
        
    return render_template('prod.html')


# @app.route(path+'/',methods=['GET','POST'])
# def handler():
#     return render_template('dev.html')



@app.route(path+'/apis/template',methods=['GET'])
def test(event=None , context= None):
    print('check', '/dev' in request.base_url)
    print('check', type(request.base_url))
    return render_template('prod.html')



# if(environ.get('FLASK_ENV')=='development'):
#     @app.route('/api/template',methods=['GET'])
    
if __name__ == '__main__':
    app.run(debug=True)
