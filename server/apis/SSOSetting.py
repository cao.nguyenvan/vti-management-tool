from flask import Blueprint, request, jsonify
import boto3
import json

from database.mutation import editWhiteListDomain, editGoogleCredential, deleteGoogleCredential, removeWhiteListDomain
from database.query import getWhiteListDomain, getSSOSetting

sso = Blueprint('sso', __name__, template_folder='../templates')

congitoIdp = boto3.client('cognito-idp')


def has_attribute(object, attribute):
    try:
        print(object[attribute])
    except:
        return False
    return True


@sso.route('/getssosetting', methods=['GET'])
def getDomainApi():
    #       return 'nothing'
    response = getSSOSetting()
    return json.dumps(response)


@sso.route('/addurlclient', methods=['POST'])
def addUrlClient():
    data = json.loads(request.get_data())
    res = congitoIdp.update_user_pool_client(
        UserPoolId=data['poolUserID'],
        ClientId=data['clientId'],
        CallbackURLs=[
            data['callbackURL']
        ],
        SupportedIdentityProviders=[
            'COGNITO', 'Google'
        ],
        LogoutURLs=[
            data['callbackURL'],
        ],
        AllowedOAuthFlowsUserPoolClient=True,
        AllowedOAuthFlows=[
            'code'
        ],
        AllowedOAuthScopes=[
            'email', 'openid'
        ])
    return jsonify(res)


@sso.route('/updatewhitelistdomain', methods=['POST'])
def updatewWitelistDomainApi():
    data = json.loads(request.get_data())
    domain = data['domain']

    response = editWhiteListDomain(domain)
    return json.dumps(response)


@sso.route('/removewhitelistdomain', methods=['POST'])
def removeWhitelistDomainApi():
    reponse = removeWhiteListDomain()
    return json.dumps(reponse)


@sso.route('/addgoogleprovider', methods=['POST'])
def addGoogleProviderApi():
    data = json.loads(request.get_data())

    res2 = congitoIdp.describe_user_pool_client(
        UserPoolId=data['poolUserID'],
        ClientId=data['clientId']
    )
    print(res2)

    res1 = congitoIdp.create_identity_provider(
        UserPoolId=data['poolUserID'],
        ProviderName='Google',
        ProviderType='Google',
        ProviderDetails={
            'client_id': data['googleClientId'],
            'client_secret': data['googleSecretId'],
            'authorize_scopes': 'email'
        },
        AttributeMapping={
            'email': 'email'
        }

    )
    editGoogleCredential(
        googleClientId=data['googleClientId'],
        googleSecretId=data['googleSecretId'])

    return jsonify(res1)


@sso.route('/removegoogleprovider', methods=['POST'])
def removeGoogleProviderApi():
    data = json.loads(request.get_data())
    congitoIdp = boto3.client('cognito-idp')

    res1 = congitoIdp.delete_identity_provider(
        UserPoolId=data['poolUserID'],
        ProviderName='Google',
    )
    deleteGoogleCredential()

    return jsonify(res1)


@sso.route('/updategoogleprovider', methods=['POST'])
def updateGoogleProviderApi():
    print('update')
    data = json.loads(request.get_data())
    res1 = congitoIdp.update_identity_provider(
        UserPoolId=data['poolUserID'],
        ProviderName='Google',
        ProviderDetails={
            'client_id': data['googleClientId'],
            'client_secret': data['googleSecretId'],
            'authorize_scopes': 'email'
        })

    editGoogleCredential(
        googleClientId=data['googleClientId'],
        googleSecretId=data['googleSecretId'])

    return jsonify(res1)
