from flask import Blueprint, request, jsonify

import boto3
import json


from database.query import getDepartments
from database.mutation import addNewDepartment , deleteDepartment , updateDepartment
department = Blueprint('department', __name__, template_folder='../templates')

@department.route('/getdepartments',methods=['GET'])
def getDepartmentsApi():
    res = getDepartments()
    print(res)
    return jsonify(res['Items'])


@department.route('/adddepartment',methods=['POST'])
def addDepartmentApi():
    data = json.loads(request.get_data())
    response = addNewDepartment(data)
    return jsonify(response)


@department.route('/deletedepartment',methods=['POST'])
def deleteDepartmentApi():
    data = json.loads(request.get_data())
    response = deleteDepartment(data)
    return jsonify(response)

@department.route('/updatedepartment',methods=['POST'])
def updateDepartmentApi():
    data = json.loads(request.get_data())
    response = updateDepartment(data)
    return jsonify(response)





