from flask import Blueprint, request, jsonify
import boto3
import json


vpc = Blueprint('vpc', __name__, template_folder='../templates')

client = boto3.client('ec2')


@vpc.route('/getdefaultvpc', methods=['GET'])
def getDefaultVpc():
    response = client.describe_vpcs(
        Filters=[
            {
                'Name': 'isDefault',
                'Values': [
                    'true',
                ]
            },
        ])

    if(len(response['Vpcs']) == 1):
        vpcId = response['Vpcs'][0]['VpcId']
        subnets = client.describe_subnets(
            Filters=[
                {
                    'Name': 'vpc-id',
                    'Values': [
                        vpcId
                    ]
                },
            ]
        )
        subnetsCidrs = [i['CidrBlock'] for i in subnets['Subnets']]
        res = {

        }
        print(len(subnets['Subnets']))
        return jsonify({
            "subnetCidrs": subnetsCidrs,
            "vpcId": vpcId
        })
    return 'There is no vpcs'
