from flask import Blueprint,request
import boto3
import ast
import json
import sys 

sys.path.insert(0, '.')
from  apis import build_response 

cloudformation = Blueprint('cloudformation', __name__, template_folder='../templates')


cloudFormationClient = boto3.client('cloudformation')

@cloudformation.route('/createstack',methods=['POST'])
def createStackApi():

    data = json.loads(request.get_data())
    #print ('check', type(data) ,data.StackName)
    response = cloudFormationClient.create_stack(
    StackName= data["StackName"],
    TemplateBody= data["TemplateBody"],
    Capabilities=[
        'CAPABILITY_IAM','CAPABILITY_NAMED_IAM','CAPABILITY_AUTO_EXPAND'
    ]
    )
    return json.dumps(response)

 
    
    