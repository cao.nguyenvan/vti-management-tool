from flask import Blueprint, request, jsonify

import boto3
import json


from database.query import getAllProjects, getProjectDetail , getProjectByDepartment
from database.mutation import addNewProject , deleteProject , editProject

project = Blueprint('project', __name__, template_folder='../templates')

@project.route('/getallprojects', methods=['GET'])
def getAllProjectApi():
    res = getAllProjects()
    print(res)
    return jsonify(res['Items'])


@project.route('/getprojectdetail', methods=['GET'])
def getProjectDetailApi():
    projectID = request.args.get('projectid')
    res = getProjectDetail(projectID)

    return jsonify(res['Item'])

@project.route('/addproject', methods=['POST'])
def addProjectApi():
    data = json.loads(request.get_data())
    res = addNewProject(data)
    return jsonify(res)

@project.route('/deleteproject', methods=['POST'])
def deleteProjectApi():
    data = json.loads(request.get_data())
    res = deleteProject(data)
    return jsonify(res)

@project.route('/getprojectbydepartment' , methods=['GET'])
def getProjectByDepartmentApi():
    department = request.args.get('department')
    res = getProjectByDepartment(department)
    return jsonify(res['Items'])

@project.route('/editproject' , methods=['POST'])
def editProjectApi():
    data = json.loads(request.get_data())
    res = editProject(data)
    return jsonify(res)



