from flask import Blueprint, request ,jsonify
import mysql.connector
import boto3
import json


admin = 'cao171'
adminpassword = 'tuilacao171'


rds = Blueprint('rds', __name__, template_folder='../templates')


@rds.route('/listuser', methods=['GET'])
def listUserRDSApi():
    host = request.args.get('host')
    mydb = mysql.connector.connect(
    host= host  ,
    user=admin,
    passwd=adminpassword
    )
    cursor = mydb.cursor()
    cursor.execute("select user from mysql.user")
    result = cursor.fetchall()
    print(result , type(result[0][0]))
    result = [ e[0].decode() for e in result]
    return jsonify(result)


@rds.route('/adduser', methods=['POST'])
def addUserRDSApi():
    data = json.loads(request.get_data())
    username = data['username']
    password = data['password']
    host = data['host']
    mydb = mysql.connector.connect(
    host= host  ,
    user= admin,
    passwd=adminpassword
    )
    cursor = mydb.cursor()
    cursor.execute("create user '{username}'@'%' identified by '{password}'".format(username=username,password=password))
    cursor.execute("grant all on `%`.* to '{username}'@'%'".format(username=username))
    cursor.execute("select user from mysql.user")
    result = cursor.fetchall()
    print(result , type(result[0][0]))
    result = [ e[0].decode() for e in result]
    return jsonify(result)
    

@rds.route('/deleteuser',methods=['POST'])
def deleteUserApi():
    data = json.loads(request.get_data())
    username = data['username']
    host = data['host']
    mydb = mysql.connector.connect(
    host= host  ,
    user= admin,
    passwd=adminpassword
    )
    cursor = mydb.cursor()
    cursor.execute("drop user '{username}'@'%'".format(username=username))
    cursor.execute("select user from mysql.user")
    result = cursor.fetchall()
    print(result , type(result[0][0]))
    result = [ e[0].decode() for e in result]
    return jsonify(result)

@rds.route('/resetpassword',methods=['POST'])
def resetPasswordApi():
    data = json.loads(request.get_data())
    username = data['username']
    password = data['password']
    host = data['host']
    mydb = mysql.connector.connect(
    host= host  ,
    user= admin,
    passwd=adminpassword
    )
    cursor = mydb.cursor()
    print (username,password)
    try:
        cursor.execute("ALTER USER '{username}'@'%' IDENTIFIED BY '{password}'".format(username=username,password=password))
    except:
        cursor.execute("SET PASSWORD FOR '{username}'@'%' = PASSWORD('{password}')".format(username=username,password=password))
        

    return jsonify(True)


