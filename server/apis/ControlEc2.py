from flask import Blueprint, request ,jsonify
import paramiko
import boto3
import json


ssh = paramiko.SSHClient()
admin = 'cao171'
password = 'tuilacao171'


ec2 = Blueprint('ec2', __name__, template_folder='../templates')


@ec2.route('/listuser', methods=['GET'])
def controlEc2Api():
    host = request.args.get('host')
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(
        host,
        username=admin,
        password=password

    )
    stdin, stdout, stderr = ssh.exec_command("sudo awk -F: '{ print $1}' /etc/passwd")
    result = stdout.readlines()
    return jsonify(result)


@ec2.route('/adduser', methods=['POST'])
def AddUserEc2():
    data = json.loads(request.get_data())
    host = data['host']
    username = data['username']
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(
        host,
        username=admin,
        password=password
    )

    stdin, stdout, stderr = ssh.exec_command("sudo useradd {username}".format(username=username))
    print(stderr.readlines())
    stdin, stdout, stderr = ssh.exec_command("sudo mkdir /home/{username}/.ssh".format(username=username))
    print(stderr.readlines())
    stdin, stdout, stderr = ssh.exec_command("sudo touch /home/{username}/.ssh/authorized_keys".format(username=username))
    print(stderr.readlines())
    stdin, stdout, stderr = ssh.exec_command("sudo ssh-keygen -f /home/{username}/.ssh/id_rsa -N \"\"".format(username=username))
    print(stderr.readlines())
    stdin, stdout, stderr = ssh.exec_command("sudo cp /home/{username}/.ssh/id_rsa.pub /home/{username}/.ssh/authorized_keys".format(username=username))
    print(stderr.readlines())
    stdin, stdout, stderr = ssh.exec_command("sudo cat /home/{username}/.ssh/id_rsa".format(username=username))
    print(stderr.readlines())
    result = stdout.readlines()
    return jsonify(result)

@ec2.route('/deleteuser',methods=['POST'])
def DeleteUser():
    data = json.loads(request.get_data())
    host = data['host']
    username = data['username']
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(
        host,
        username=admin,
        password=password
    )

    stdin, stdout, stderr = ssh.exec_command("sudo userdel --remove -f {username}".format(username=username))
    print(stderr)
    stdin, stdout, stderr = ssh.exec_command("sudo awk -F: '{ print $1}' /etc/passwd")
    print(stderr)
    result = stdout.readlines()
    return jsonify(result)

@ec2.route('/getpemkey',methods=['GET'])
def GetPemKey():
    host = request.args.get('host')
    username = request.args.get('username')
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(
        host,
        username=admin,
        password=password

    )

    stdin, stdout, stderr = ssh.exec_command("sudo cat /home/{username}/.ssh/id_rsa".format(username=username))
    print('check', stdin, stdout, stderr)
    result = stdout.readlines()
    return jsonify(result)


