from flask import Blueprint, request, jsonify
import paramiko
import boto3
import json

user = Blueprint('user', __name__, template_folder='../templates')

cognitoIdp = boto3.client('cognito-idp')
iam = boto3.client('iam')
adminGroupName = 'SuperAdmin'


def returnUser(e):
    user = {}
    user['username'] = e['Username']
    for i in e['Attributes']:
        user[i['Name']] = i['Value']
    return user
def has_attribute(object, attribute):
    try:
        a = (object[attribute])
    except:
        return False
    return True


@user.route('/listusersnormal', methods=['GET'])
def listUsersNormalApi():
    userPoolID = request.args.get('userpoolid')
    res1 = cognitoIdp.list_users(
        UserPoolId=userPoolID)

    users = [returnUser(el) for el in res1['Users']]
    return jsonify(users)


@user.route('/getuser', methods=['GET'])
def getUser():
    username = request.args.get('username')
    userPoolID = request.args.get('poolid')
    res1 = cognitoIdp.list_users(
        UserPoolId=userPoolID,
        Username=username
        )
    return jsonify()
    
@user.route('/listusers', methods=['GET'])
def listUsersApi():
    userPoolID = request.args.get('userpoolid')
    res1 = cognitoIdp.list_users(
        UserPoolId=userPoolID)

    users = [returnUser(el) for el in res1['Users']]
    res2 = cognitoIdp.list_users_in_group(
        UserPoolId=userPoolID,
        GroupName=adminGroupName
    )

    admin = [returnUser(el)['sub'] for el in res2['Users']]

    def returnUserWithAdminAttribute(e):
        user = e
        if(user['sub'] in admin):
            user['admin'] = True
        else:
            user['admin'] = False
        return user

    return jsonify([returnUserWithAdminAttribute(e) for e in users])


@user.route('/admingroup', methods=['POST'])
def adminGroupApi():
    data = json.loads(request.get_data())
    userPoolID = data['userPoolId']
    userName = data['userName']
    if (data['add']):
        res = cognitoIdp.admin_add_user_to_group(
            UserPoolId=userPoolID,
            Username=userName,
            GroupName=adminGroupName
        )
    else:
        res = cognitoIdp.admin_remove_user_from_group(
            UserPoolId=userPoolID,
            Username=userName,
            GroupName=adminGroupName
        )
    return jsonify(res)


@user.route('/deleteuser', methods=['POST'])
def deleteUserApi():
    data = json.loads(request.get_data())
    userPoolID = data['userPoolId']
    userName = data['userName']
    res = cognitoIdp.admin_delete_user(
        UserPoolId=userPoolID,
        Username=userName
    )

    return jsonify(res)


@user.route('/adduser', methods=['POST'])
def addUserApi():
    data = json.loads(request.get_data())
    userPoolID = data['userPoolId']
    userName = data['userName']
    password = data['password']
    res = cognitoIdp.admin_create_user(
        UserPoolId=userPoolID,
        Username=userName,
        UserAttributes=[
            {
                'Name': 'email',
                'Value': userName
            }
        ],
        TemporaryPassword=password
    )

    if(data['admin']):
        cognitoIdp.admin_add_user_to_group(
            UserPoolId=userPoolID,
            Username=userName,
            GroupName=adminGroupName
        )

    return jsonify(returnUser(res['User']))


@user.route('/checkadmin', methods=['GET'])
def checkAdminApi():
    userPoolID = request.args.get('userpoolid')
    userName = request.args.get('username')
    print('checkkk ', userPoolID, userName)

    response = cognitoIdp.admin_list_groups_for_user(
        Username=userName,
        UserPoolId=userPoolID
    )


    print('check', response['Groups'])
    def getRoleArn(e):
        if(has_attribute(e,'RoleArn')):
            return e['RoleArn']
        return None
    roleArns = [getRoleArn(e) for e in response['Groups']]

    def getPolicyDocument(policyName, roleName):
        response = iam.get_role_policy(
            RoleName=roleName,
            PolicyName=policyName
        )
        return response['PolicyDocument']['Statement']

    for i in roleArns:
        print('check i', i)
        if(i== None): 
            break
        roleName = i.split('/')[1]
        res = iam.list_role_policies(
            RoleName=roleName
        )

        for policy in res['PolicyNames']:
            document = getPolicyDocument(policy, roleName)
            print('check document',document)
            for statement in document:
                if(statement['Action']=='*' and statement['Resource']=='*' and statement['Effect']=='Allow'):
                    return jsonify(True)
    return jsonify(False)
