def build_response(**data):
    """
    Builds api response data
    :param data: the metrics data
    :return:
    """
    metrics = {
    }
    for d in data:
        metrics[d] = data[d]

    return metrics
