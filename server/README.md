# Install Notes:

### Assume that you're using MAC and Linux

### First you need install
 python 3.6+
 virtualenv
 node.js
 yarn 
 aws cli ( and add your accessToken and SecretToken )

### Create virtualenv to work with python

- Go into 'server' folder:
  cd server

- Run to init virtual environment:
  virtualenv -p python3 venv

- Go into virtual environment:
  source ./venv/bin/activate

- NOTICE!! You need to work in virtual environment in that project and run 'make' command in 'server' folder

### Change project_name in '/server/zappa_settings.txt' to "your_project_name"

### Install dependencies and packages
- Run :
    make install


# Run Notes:
- Run server:
    make runserver
- Run client:
    make runclient

# Deploy Notes:
- Run :
    make allbuild

# Update Notes:
- Run:
    make allupdate



