
env = dev
buildapp:
	export FLASK_ENV=production
	mkdir ./server/static
	mkdir ./server/templates
	mkdir ./server/static/prod
	mkdir ./server/static/dev


	## For prod app
	yarn --cwd ./client  build
	cp -r ./client/build/assets     ./server/static/prod/assets
	cp -r ./client/build/static/*   ./server/static/prod
	cp -r ./client/build/manifest.json ./server/static/manifest.json
	cp -r ./client/build/index.html  ./server/templates/prod.html
	sed -i -e 's/static\//static\/prod\//g' ./server/templates/prod.html
	sed -i -e 's/assets\//static\/prod\/assets\//g' ./server/templates/prod.html
	sed -i -e 's/\/manifest.json/\/static\/manifest.json/g' ./server/templates/prod.html

	## For dev app
	rm -rf ./client/build
	sed -i -e 's/modePath=""/modePath="dev\/"/g' ./client/config/webpack.config.js
	yarn --cwd ./client build
	sed -i -e 's/modePath="dev\/"/modePath=""/g' ./client/config/webpack.config.js
	cp -r ./client/build/assets     ./server/static/dev/assets
	cp -r ./client/build/dev/static/*   ./server/static/dev
	cp -r ./client/build/index.html  ./server/templates/dev.html
	sed -i -e 's/static\//static\/dev\//g' ./server/templates/dev.html
	sed -i -e 's/assets\//dev\/static\/dev\/assets\//g' ./server/templates/dev.html
	sed -i -e 's/\/manifest.json/\/dev\/static\/manifest.json/g' ./server/templates/dev.html


initenv:
	python3 ./server/cli.py init 

clean:
	rm -rf ./server/static
	rm -rf ./server/templates

deploy:
	cd server;\
	python3 ./zappa/cli.py deploy dev 

update:
	cd server;\
	python3 ./zappa/cli.py update dev 

undeploy:
	cd server;\
	python3  ./zappa/cli.py undeploy dev
	cd .. ;\

allbuild: clean  buildapp deploy

allupdate: clean buildapp update

allundeploy: 
	python3 ./init/removeInit.py
	make undeploy

initapi:
	cd server;\
	python3 ./zappa/cli.py init 
setupvenv:
	virtualenv -p python3 ./venv	

install:
	yarn --cwd ./client install
	pip install -r requirements.txt


setupawsenv:
	python3 ./init/runConfig.py


runserver:
	cd server ;\
	FLASK_ENV=development flask run --port 5000 

runclient:
	yarn --cwd ./client start

    









