import boto3
import json

TableName2 = 'VTI-Management-Setting-1'


def has_attribute(object, attribute):
    try:
        a = (object[attribute])
    except:
        return False
    return True


def lambda_handler(event, context):
    dynamodb = boto3.resource('dynamodb')
    cognitoIdp = boto3.client('cognito-idp')

    users = cognitoIdp.list_users(
        UserPoolId=event['userPoolId'],
        Filter='email = "{email}"'.format(
            email=event['request']['userAttributes']['email'])
    )
    users = users['Users']
    
    print('check resource users', event)
    
    print('check users',users)

    if (len(users) == 1):
        def validateUser(user):
            validUser = {}
            validUser['Username'] = user['Username']
            for att in user['Attributes']:
                validUser[att['Name']] = att['Value']
            return validUser

        validUser = [validateUser(user) for user in users][0]

        print('check destination user ', validUser)
        # raise Exception('Test den day thoi')
        
        # print('check',validUser['identities'][0], validUser['identities'])

        # print(test)
        if(not has_attribute(validUser, 'identities')):
            print('check resource user ' ,event['userName'].split('_')[0], event['userName'].split('_')[1]
            , event['userPoolId'], validUser['Username'])
            cognitoIdp.admin_link_provider_for_user(
                UserPoolId=event['userPoolId'],
                DestinationUser={
                    'ProviderName': 'Cognito',
                    'ProviderAttributeValue': validUser['Username']
                },
                SourceUser={
                    'ProviderName': event['userName'].split('_')[0],
                    'ProviderAttributeName': 'Cognito_Subject',
                    'ProviderAttributeValue': event['userName'].split('_')[1]
                }
            )
            return json.dumps(None)
            # res = cognitoIdp.admin_link_provider_for_user(
            #     UserPoolId='ap-northeast-1_Mb7axiOD2',
            #     DestinationUser={
            #         'ProviderName': 'Cognito',
            #         'ProviderAttributeValue': 'caobo1712000@gmail.com'
            #     },
            #     SourceUser={
            #         'ProviderName': 'Google',
            #         'ProviderAttributeName': 'Cognito_Subject',
            #         'ProviderAttributeValue': '105938216568468174257'
            #     }
            # )
        else:
            print('check voday roi ')
            identities = json.loads(validUser['identities'])
            cognitoIdp.admin_link_provider_for_user(
                UserPoolId=event['userPoolId'],
                DestinationUser={
                    'ProviderName': identities[0]['providerName'],
                    'ProviderAttributeValue': identities[0]['userId']
                },

                SourceUser={
                    'ProviderName': event['userName'].split('_')[0],
                    'ProviderAttributeName': 'Cognito_Subject',
                    'ProviderAttributeValue': event['userName'].split('_')[1]
                }
            )

    # CHeck whiltelist !!
    table2 = dynamodb.Table(TableName2)
    response = table2.get_item(
        Key={
            'pk': 'SETTING_PROPERTY',
            'sk': 'whitelistdomain'
        }

    )
    whitelist = None
    if event['userName'].split('_')[0] == "Google":

        if(not has_attribute(response, 'Item')):
            raise Exception(
                "Cannot authenticate users from this type of domain")
        else:
            whitelist = response['Item']['domainvalue']
            domainOfUser = event['request']['userAttributes']['email'].split('@')[
                1]
            if domainOfUser in whitelist:
                return event
            else:
                raise Exception(
                    "Cannot authenticate users from this type of domain")

    # Return to Amazon Cognito
    return event


if __name__ == "__main__":

    res = lambda_handler(
        {
            "version": "1",
            "region": "ap-northeast-1",
            "userPoolId": "ap-northeast-1_Mb7axiOD2",
            "userName": "Google_102654460151538205039",
            "callerContext": {
                "awsSdkVersion": "aws-sdk-unknown-unknown",
                "clientId": "4q2vo8r7j7dma80beif149oem"
            },
            "triggerSource": "PreSignUp_ExternalProvider",
            "request": {
                "userAttributes": {
                    "cognito:email_alias": "",
                    "cognito:phone_number_alias": "",
                    "email": "caobo1712000@gmail.com"
                },
                "validationData": {}
            },
            "response": {
                "autoConfirmUser": False,
                "autoVerifyEmail": False,
                "autoVerifyPhone": False
            }
        }, {})

#    print(res)
