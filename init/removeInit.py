from outputs import STACK_ARN, POOL_ID, FUNCTION_NAME, DOMAIN
import boto3
cognito = boto3.client('cognito-idp')
iam = boto3.client('iam')
lambda_aws = boto3.client('lambda')
cloudformation = boto3.client('cloudformation')


if __name__ == "__main__":
    try:
        lambda_aws.delete_function(
            FunctionName=FUNCTION_NAME
        )
    except:
        print('ok')

    print('delete preSignup function')
    try:
        cognito.delete_user_pool_domain(
            Domain=DOMAIN,
            UserPoolId=POOL_ID
        )
    except:
        print('ok')

    print('delete user pool domain')
    try:
        cognito.delete_user_pool(
            UserPoolId=POOL_ID
        )
    except:
        print('ok')
    print('delete user pool ')

    cloudformation.delete_stack(
        StackName=STACK_ARN
    )
    print('delete cloudformation stack')
