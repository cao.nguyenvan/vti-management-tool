from os.path import expanduser

from createUserPool import setupUserCognito
from config import DOMAIN


home = expanduser("~")


def getAWSConfig():
    f = open('{}/.aws/credentials'.format(home), 'r')
    credentialsArray = f.read().split('\n')
    for element in credentialsArray:
        array = element.split('=')
        if(array[0].strip() != '[default]' and array[0].strip() != ''):
            if(len(array) == 1):
                print('Please configure your AWS Credentials First !!')
                print("Run \033[1;32;40m aws configure  \n")


def setupConfig():
    

    print('Please enter your Google Authentication Credentials for your app')
    
    userName = input('SUPER_ADMIN_EMAIL : ')
    password = input('SUPER_ADMIN_PASSWORD : ')
    
    appClientId ,poolId, functionName , stackArn = setupUserCognito( userName, password)

    f = open('./client/src/initFromServer.js', 'w+')
    f.write("export const APP_CLIENT_ID = '{app_client_id}'\nexport const DOMAIN_FROM_SERVER = '{domain}' \nexport const POOL_ID = '{pool_id}'"
        .format(
        app_client_id = appClientId ,
        domain = DOMAIN ,
        pool_id = poolId
        ))
    
    f.close()

    f1 = open('./init/outputs.py','w+')
    f1.write("STACK_ARN = '{stack_arn}'\nPOOL_ID = '{pool_id}'\nFUNCTION_NAME= '{function_name}'\nDOMAIN = '{domain}'"
        .format(
            stack_arn = stackArn,
            pool_id = poolId,
            function_name = functionName,
            domain = DOMAIN
        ))






if (__name__ == '__main__'):

    getAWSConfig()
    setupConfig()
    print('All done !!')
    
