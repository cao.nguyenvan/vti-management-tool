import boto3
import json
import time


def initTemplate():
    template = {
        "AWSTemplateFormatVersion": "2010-09-09",
        "Description": "Initialize resources for the vti management tool",
        "Parameters": {},
        "Resources": {
            "VTIManagementDatabase": {
                "Type": "AWS::DynamoDB::Table",
                "Properties": {
                    "TableName": "VTI-Management-Database-1",
                    "BillingMode": "PAY_PER_REQUEST",
                    "AttributeDefinitions": [
                        {
                            "AttributeName": "pk",
                            "AttributeType": "S"
                        },
                        {
                            "AttributeName": "sk",
                            "AttributeType": "S"
                        },
                        {
                            "AttributeName": "datavalue",
                            "AttributeType": "S"
                        }
                    ],
                    "KeySchema": [
                        {
                            "AttributeName": "pk",
                            "KeyType": "HASH"
                        },
                        {
                            "AttributeName": "sk",
                            "KeyType": "RANGE"
                        }
                    ],
                    "GlobalSecondaryIndexes": [
                        {
                            "IndexName": "gsi",
                            "KeySchema": [
                                {
                                    "AttributeName": "sk",
                                    "KeyType": "HASH"
                                },
                                {
                                    "AttributeName": "datavalue",
                                    "KeyType": "RANGE"
                                }
                            ],
                            "Projection": {
                                "ProjectionType": "ALL"
                            }
                        }
                    ]
                }
            },
            "VTIManagementSetting": {
                "Type": "AWS::DynamoDB::Table",
                "Properties": {
                    "TableName": "VTI-Management-Setting-1",
                    "BillingMode": "PAY_PER_REQUEST",
                    "KeySchema": [
                        {
                            "AttributeName": "pk",
                            "KeyType": "HASH"
                        },
                        {
                            "AttributeName": "sk",
                            "KeyType": "RANGE"
                        }
                    ],
                    "AttributeDefinitions": [
                        {
                            "AttributeName": "pk",
                            "AttributeType": "S"
                        },
                        {
                            "AttributeName": "sk",
                            "AttributeType": "S"
                        }
                    ]
                }
            },
            "DynamoLambdaAccessRole": {
                "Type": "AWS::IAM::Role",
                "DependsOn": "VTIManagementSetting",
                "Properties": {
                    "AssumeRolePolicyDocument": {
                        "Version": "2012-10-17",
                        "Statement": [
                            {
                                "Effect": "Allow",
                                "Principal": {
                                    "Service": [
                                        "lambda.amazonaws.com"
                                    ]
                                },
                                "Action": [
                                    "sts:AssumeRole"
                                ]
                            }
                        ]
                    },
                    "Policies": [
                        {
                            "PolicyName": "DynamoLambdaAccessPolicy",
                            "PolicyDocument": {
                                "Version": "2012-10-17",
                                "Statement": [
                                    {
                                        "Effect": "Allow",
                                        "Action": "dynamodb:GetItem",
                                        "Resource": {
                                            "Fn::GetAtt": [
                                                "VTIManagementSetting",
                                                "Arn"
                                            ]
                                        }
                                    },
                                    {
                                        "Effect": "Allow",
                                        "Action": [
                                            "logs:CreateLogGroup",
                                            "logs:CreateLogStream",
                                            "logs:PutLogEvents"
                                        ],
                                        "Resource": "arn:aws:logs:*:*:*"
                                    },{
                                        "Effect": "Allow",
                                        "Action":[
                                            "cognito-idp:*"
                                        ],
                                        "Resource": "arn:aws:cognito-idp:*:*:*"
                                    }
                                ]
                            }
                        }
                    ]
                }
            },

            "ManagementAdminRole": {
                "Type": "AWS::IAM::Role",
                "Properties": {
                    "AssumeRolePolicyDocument": {
                        "Version": "2012-10-17",
                        "Statement": [
                            {
                                "Effect": "Allow",
                                "Principal": {
                                    "Service": [
                                        "cognito-idp.amazonaws.com"
                                    ]
                                },
                                "Action": [
                                    "sts:AssumeRole"
                                ]
                            }
                        ]
                    },
                    "Policies": [
                        {
                            "PolicyName": "ManagementAdminPolicy",
                            "PolicyDocument": {
                                "Version": "2012-10-17",
                                "Statement": [
                                    {
                                        "Effect": "Allow",
                                        "Action": "*",
                                        "Resource": "*"
                                    }
                                ]
                            }
                        }
                    ]
                }
            }
        },
        "Outputs": {
            "PreLoginFunctionRole": {
                "Value": {
                    "Fn::GetAtt": [
                        "DynamoLambdaAccessRole",
                        "Arn"
                    ]
                }
            },
            "AdminRole": {
                "Value": {
                    "Fn::GetAtt": [
                        "ManagementAdminRole",
                        "Arn"
                    ]
                }
            }
        }
    }

    cloudFormation = boto3.client('cloudformation')
    templateData = json.dumps(template)
    # print ('check', type(data) ,data.StackName)
    response = cloudFormation.create_stack(
        StackName='VTI-Management-Init-Stack-1',
        TemplateBody=templateData,
        Capabilities=[
            'CAPABILITY_IAM', 'CAPABILITY_NAMED_IAM', 'CAPABILITY_AUTO_EXPAND'
        ]
    )

    response2 = cloudFormation.describe_stacks(
        StackName=response['StackId']
    )
    Stack = response2['Stacks'][0]

    print('waiting for init template ...')
    time.sleep(30)
    while(True):
        print(Stack)
        response2 = cloudFormation.describe_stacks(
            StackName=response['StackId']
        )
        Stack = response2['Stacks'][0]
        if(Stack['StackStatus'] == 'CREATE_COMPLETE'):
            print(Stack)
            roleArns = response2['Stacks'][0]['Outputs']
            roleArn = {}
            for i in roleArns:
                roleArn[i['OutputKey']] = i['OutputValue']
            return roleArn['AdminRole'], roleArn['PreLoginFunctionRole'], response['StackId']
        elif(Stack['StackStatus'] == 'ROLLBACK_COMPLETE'):
            print(Stack['StackStatus'])
            return None
        else:
            print('waaiiit')
            time.sleep(30)


if __name__ == "__main__":
    res = initTemplate()
    print('check result', res)
