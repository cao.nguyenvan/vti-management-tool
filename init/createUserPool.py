import boto3
import json
from zipfile import ZipFile
import os
import io
import random

from config import ZIP_PRELOGIN_FUNCITON_FILE, DOMAIN
from initTemplate import initTemplate


client = boto3.client('cognito-idp')
iam = boto3.client('iam')
lambda_aws = boto3.client('lambda')
groupName = 'SuperAdmin'
poolName = 'VTI-Management-user'


def createPreLoginFunction(roleArn):
    # with ZipFile(ZIP_PRELOGIN_FUNCITON_FILE,'w') as zip:
    #     # writing each file one by one
    #     zip.write('./preLogin.py')
    # f = open('../init/preSignUp.py', 'r')
    # print(f.read())

    def readFile():
        buf = io.BytesIO()
        with ZipFile(buf, 'w') as z:
            z.write('./init/preSignUp.py', 'preSignUp.py')
        buf.seek(0)
        return buf.read()

    def findElement(rolename, array):
        for el in array:
            # print(el)
            if(el['RoleName'] == rolename):
                return el
        return None

    lambdaname = 'preSignUP' + str(random.randint(0, 100))
    response = lambda_aws.create_function(
        FunctionName=lambdaname,
        Role= roleArn,
        Runtime='python3.6',
        Handler='preSignUp.lambda_handler',
        Code={
            'ZipFile': readFile()
        })
    print('response', response)
    return response


def createUserPool():
    adminRole , preLoginFunctionRole , stackArn = initTemplate()
    function = createPreLoginFunction(preLoginFunctionRole)

    response = client.create_user_pool(
        PoolName=poolName,
        Policies={
            'PasswordPolicy': {
                'MinimumLength': 6
            }
        },
        MfaConfiguration='OFF',
        LambdaConfig={
            'PreSignUp': function['FunctionArn']
        },
        AliasAttributes=[
            'preferred_username',
        ],
        Schema=[
            {
                'Name': 'email',
                'AttributeDataType': 'String',
                'Required': True
            }
        ]
    )
    lambda_aws.add_permission(
        FunctionName=function['FunctionName'],
        StatementId='cao-test-baldjagl12123',
        Action='lambda:InvokeFunction',
        Principal='cognito-idp.amazonaws.com',
        SourceArn=response['UserPool']['Arn']
    )


    print('created user pool "{}" successful !! '.format(poolName))
    return response['UserPool']['Id'] , function['FunctionName'] , stackArn , adminRole


def createAppClient(poolId):
    response = client.create_user_pool_client(
        UserPoolId=poolId,
        ClientName='string',
        SupportedIdentityProviders=[
            'COGNITO'
        ]
       
    )
    print('added app client !!')
    return response['UserPoolClient']['ClientId']


def createUserPoolDomain(poolId):
    response = client.create_user_pool_domain(
        Domain=DOMAIN,
        UserPoolId=poolId,
    )
    return response


def createGoogleAuthentication(poolId, googleClientId, googleSecretId):
    response = client.create_identity_provider(
        UserPoolId=poolId,
        ProviderName='Google',
        ProviderType='Google',
        ProviderDetails={
            'client_id': googleClientId,
            'client_secret': googleSecretId,
            'authorize_scopes': 'email'
        },
        AttributeMapping={
            'email': 'email'
        }

    )
    print('added google authentication provider !!')


def createUserAndAddUserToIAMGroup(username):
    iam.create_group(
        GroupName=groupName
    )
    iam.create_user(
        UserName=username,
    )
    iam.add_user_to_group(
        GroupName='SuperAdmin',
        UserName=username
    )
    print('added iam "{}"!!'.format(groupName))


def createFirstAdmin(poolId, username, password , roleArn):

    client.create_group(
        GroupName=groupName,
        UserPoolId=poolId,
        Description='The group includes superadmins of the app',
        Precedence=23, 
        RoleArn= roleArn
    )

    client.admin_create_user(
        UserPoolId=poolId,
        Username=username,
        UserAttributes=[
            {
                'Name': 'email',
                'Value': username
            }
        ],
        TemporaryPassword=password
    )

    client.admin_add_user_to_group(
        UserPoolId=poolId,
        Username=username,
        GroupName=groupName
    )
    print('added cognito group "{}"!!'.format(groupName))


def setupUserCognito(username, password):
    poolId , functionName , stackArn ,  adminRole = createUserPool()
    appClientId = createAppClient(poolId)
    createUserPoolDomain(poolId)
    # createUserAndAddUserToIAMGroup(username)
    createFirstAdmin(poolId, username, password , adminRole )
    return appClientId, poolId , functionName , stackArn


if __name__ == "__main__":
    createPreLoginFunction('arn:aws:iam::555516925462:role/lambda_basic_execution')
