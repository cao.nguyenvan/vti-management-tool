## Để set up môi trường thì 
 
- Bạn chạy
   make setupvenv 
để thiết lập virtual environment trong dự án
- Chạy source ./venv/bin/activate ( Linux ) để làm việc trong virtualenv
- Chạy make install để cài đật các package và dependencies trong client và server
- Chạy make setupawsenv để khởi tạo các tài nguyên như userpool trong cognito và dynamodb


## Để deploy thì bạn chạy lệnh 
- Chạy make allbuild ( nếu xảy ra lỗi chưa init zappa_settings thì bạn chạy make init để khởi tạo )

## Để  update project đã deploy 
- Chạy  make allupdate 

