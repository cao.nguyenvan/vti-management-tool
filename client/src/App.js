import React, { Component } from "react";
import { Provider, SubscribeOne } from "unstated-x";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import SignIn from "./components/SignIn/SignIn";
import Amplify, { Auth, Hub } from "aws-amplify";
import { AMPLIFY_CONFIG, oauth } from "./config";


import loadingContainer from './containers/LoadingContainer'


import { BarLoader, GridLoader, ScaleLoader, CircleLoader } from 'react-spinners'
import { css } from '@emotion/core';




import Main from './components/Main'
import Dashboard from './components/Dashboard/Dashboard'
import ProjectList from './components/Project/ProjectList'
import ProjectDetail from './components/Project/ProjectDetail'
import SideNav from './components/SideNav/SideNav'
import AddProject from './components/Project/AddProject'
import NotFound from './components/Common/NotFound'
import Header from './components/Common/Header'
import GeneralSettings from './components/Setting/GeneralSettings'
import User from './components/Setting/Users'



import authContainer from './containers/AuthContainer'
import { checkAdmin } from './helpers/utils'
import { addCallbackUrlToAppClient } from './helpers/utils'



Amplify.configure(AMPLIFY_CONFIG);
Auth.configure({ oauth });

class App extends Component {
  constructor(props) {

    super(props);
    this.path = window.location.href.indexOf('/dev') !== -1 || window.location.hostname ==='localhost'? '/dev' : ''
    this.onHubCapsule = this.onHubCapsule.bind(this);
    // let the Hub module listen on Auth events
    Hub.listen("auth", this);
    this.state = {
      authState: "loading"
    };
  }

  componentDidMount() {

    addCallbackUrlToAppClient().then(result => {
      console.log('check result', result)
    })
    const urlParams = new URLSearchParams(window.location.search)
    const key = urlParams.get('error_description')
    if (key && key.split(' ').join('') === 'Unrecognizablelambdaoutput') {
      alert('Linked user successfull ! Please Sign in Again !')
    }

    Auth.currentAuthenticatedUser()
      .then(async (user) => {

        if (this.state.authState !== "signedIn") {
          this.setState({ authState: "signedIn" }, async () => {
            checkAdmin(user).then(admin => {
              authContainer.setState({ admin })
            })

          });
        }
      })
      .catch(e => {
        this.setState({ authState: "signIn" });
      });
  }

  onHubCapsule(capsule) {
    // The Auth module will emit events when user signs in, signs out, etc
    const { channel, payload: { event, data } } = capsule;
    window.capsule = capsule
    if (channel === "auth") {
      switch (event) {
        case "signIn":
          if (this.state.authState !== "signedIn") {

            this.setState({ authState: "signedIn" }, async () => {
              checkAdmin(data).then(admin => {
                authContainer.setState({ admin })
              })

            });
          }
          break;
        case "signIn_failure":
          this.setState({ authState: "signIn" });
          break;
        default:
          break;
      }
    }
  }

  render() {
    const { authState } = this.state;

    return (
      <Provider>
        <Router>
          <Switch>
            {authState === "signIn" &&
              <Route path={`${this.path}/`} component={SignIn}></Route>
            }
            {authState === "signedIn" && (

              <React.Fragment>
                <Route path={`${this.path}/`} exact component={() => (
                  <SidebarWrapper>
                    <Dashboard></Dashboard>
                  </SidebarWrapper>
                )}></Route>

                <Route path={`${this.path}/generalsettings`} exact component={() => (
                  <SidebarWrapper>
                    <GeneralSettings></GeneralSettings>
                  </SidebarWrapper>
                )}></Route>

                <Route path={`${this.path}/projectdetail`} exact component={() => (
                  <SidebarWrapper>
                    <ProjectDetail></ProjectDetail>
                  </SidebarWrapper>
                )}></Route>

                <Route path={`${this.path}/project`} exact component={() => (
                  <SidebarWrapper>
                    <ProjectList></ProjectList>
                  </SidebarWrapper>
                )}></Route>

                <Route path={`${this.path}/addproject`} exact component={() => (
                  <SidebarWrapper>
                    <AddProject></AddProject>
                  </SidebarWrapper>
                )}></Route>

                <SubscribeOne to={authContainer} bind={['admin']}>
                  {
                    auth => {
                      return (
                        auth.state.admin && (
                          <Route path={`${this.path}/user`} exact component={() => (
                            <SidebarWrapper>
                              <User></User>
                            </SidebarWrapper>
                          )}></Route>
                        )
                      )
                    }
                  }
                </SubscribeOne>

                <Route path={`${this.path}/design`} exact component={Main}></Route>
                <Route component={NotFound} />
              </React.Fragment>
            )}
          </Switch>
        </Router >
        <ToastContainer />

      </Provider >
    );
  }
}


const override = `
    display: block;
    margin-left: 45%;
    border-width:10px;
    margin-top:20%;
    transform: translate(-50%, -50%);
    
`;

class SidebarWrapper extends React.Component {
  render() {
    return (
      <React.Fragment>
        <SideNav />
        <div id="right-panel" className="right-panel">
          <Header></Header>
          <div className="content">
            <div className="animated fadeIn">
              <SubscribeOne to={loadingContainer} bind={['loading']}>
                {
                  loadCon => {
                    return (
                      <React.Fragment>

                        <CircleLoader
                          css={override}
                          sizeUnit={"px"}
                          size={150}
                          color={'#123abc'}
                          width={'100%'}
                          loading={loadCon.state.loading}
                        />

                        <div
                          style={{ display: loadCon.state.loading ? 'none' : 'block' }}
                        >
                          {this.props.children}
                        </div>
                      </React.Fragment>)

                  }}
              </SubscribeOne>

            </div>
          </div>
        </div>


      </React.Fragment>
    )
  }
}


export const rootPath = window.location.href.indexOf('/dev') !== -1 || window.location.hostname ==='localhost'? '/dev' : ''
export const ComponentHierachy = [
  {
    'Name': 'Project',
    'Path': `${rootPath}/project`
  },
  {
    'Name': 'System',
    'Children': [2, 3]
  },
  {
    'Name': 'General Settings',
    'Path': `${rootPath}/generalsettings`,
    'Parent': 1
  },
  {
    'Name': 'Users',
    'Path': `${rootPath}/user`,
    'Parent': 1
  }
]
export default App;
