// GOOGLE
import {  APP_CLIENT_ID, DOMAIN_FROM_SERVER ,POOL_ID } from './initFromServer'
// AWS COGNITO
// export const IDENTITY_POOL_ID =
//   "ap-northeast-1:974e6845-8d18-4b98-aa94-53879c85e022";
export const IDENTITY_POOL_REGION = "ap-northeast-1";

export const USER_POOL_ID = POOL_ID;
export const USER_POOL_WEB_CLIENT_ID = APP_CLIENT_ID;
export const REGION = "ap-northeast-1";

const path =  window.location.href.indexOf('/dev') !== -1 ? '/dev' : ''
const DOMAIN = `${DOMAIN_FROM_SERVER}.auth.ap-northeast-1.amazoncognito.com`;
export const REDIRECT_SIGNIN = `${window.location.origin}${path}`;
const RESPONSE_TYPE = "code";

export const URL_TO_GOOGLE =

  "https://" +
  DOMAIN +
  "/oauth2/authorize?redirect_uri=" +
  REDIRECT_SIGNIN +
  "&response_type=" +
  RESPONSE_TYPE +
  "&client_id=" +
  USER_POOL_WEB_CLIENT_ID +
  "&identity_provider=Google";

  window.urlm = URL_TO_GOOGLE

export const AMPLIFY_CONFIG = {
  // REQUIRED only for Federated Authentication - Amazon Cognito Identity Pool ID
  identityPoolId: '',
  // REQUIRED - Amazon Cognito Region
  region: REGION,
  // OPTIONAL - Amazon Cognito Federated Identity Pool Region
  // Required only if it's different from Amazon Cognito Region
  identityPoolRegion: IDENTITY_POOL_REGION,
  // OPTIONAL - Amazon Cognito User Pool ID
  userPoolId: USER_POOL_ID,
  // OPTIONAL - Amazon Cognito Web Client ID (26-char alphanumeric string)
  userPoolWebClientId: USER_POOL_WEB_CLIENT_ID
};

// Cognito Hosted UI configuration
export const oauth = {
  domain: DOMAIN,
  scope: [
    "phone",
    "email",
    "profile",
    "openid",
    "aws.cognito.signin.user.admin"
  ],
  redirectSignIn: REDIRECT_SIGNIN,
  redirectSignOut: REDIRECT_SIGNIN,
  responseType: RESPONSE_TYPE
};

window.redirect = REDIRECT_SIGNIN
console.log('checkk redirect', REDIRECT_SIGNIN)
