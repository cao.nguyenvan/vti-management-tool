import React from 'react'
import ElementContainer, { PathContainer } from '../../containers/ElementContainer'
import workspaceContainer, { refSVG } from '../../containers/WorkspaceContainer'
import templateContainer from '../../containers/TemplateContainer'
import { SubscribeOne } from 'unstated-x';
import renderPath from '../../helpers/renderPath'


class PathBox extends React.Component {
    state = {
        dataMovedOverElement: null,

        onMouseDown: false,
        pathStart: null,
        pathEnd: null,
        isCreatingPath: false,


        editPathStart: false,
        pathElement: null,
        prevPath: { x: 0, y: 0 }

    }

    updateStartPointOfPath = (e, pathElement) => {
        const pathContainer = PathContainer.get(pathElement)
        const rectSvg = refSVG.current.getBoundingClientRect()

        pathContainer.setState((state, props) => {
            return {
                peak: {
                    x: e.clientX - rectSvg.left - (state.start.x + state.end.x) / 2,
                    y: e.clientY - rectSvg.top - (state.start.y + state.end.y) / 2
                }
            }
        })
    }

    updateEndPointOfPath = (e) => {

        const rectSvg = refSVG.current.getBoundingClientRect()
        const pathEnd = {
            x: e.clientX + 4 - rectSvg.left,
            y: e.clientY + 4 - rectSvg.top
        }
        this.setState({ pathEnd })
    }

    startCreatePath = (x, y, point) => {
        const pathStart = { x, y }
        this.setState({ pathStart, pathEnd: pathStart, startId: point, isCreatingPath: true, onMouseDown: true })
    }

    selectPath = (e, path) => {
        workspaceContainer.setState({ selected: path })
        this.setState({ editPathStart: true, pathElement: path }, () => {
            this.prevX = e.clientX;
            this.prevY = e.clientY;
            PathContainer.get(path).dom.setAttribute('stroke', 'red')
        })
    }


    finishCreatePath = (e) => {
        const data = e.target.getAttribute('data-bounding')
        if (data && data !== this.state.startId) {
            PathContainer.addPath({
                start: this.state.pathStart,
                end: this.state.pathEnd,
                point1: this.state.startId,
                point2: data,
                peak: { x: 0, y: 0 }
            })

            templateContainer.setState({ paths: Array.from(PathContainer.instances) })
        }

        if (this.state.pathElement) {
            PathContainer.get(this.state.pathElement).dom.setAttribute('stroke', 'blue')
        }
        this.setState({
            onMouseDown: false,
            isCreatingPath: false,
            pathEnd: null,
            pathStart: null,
            editPathStart: false, pathElement: null
        })
    }


    convertToBox(svgBox, elementBox) {
        const box = {
            top: elementBox.top - svgBox.top,
            left: elementBox.left - svgBox.left,
            bottom: elementBox.top - svgBox.top + elementBox.height,
            right: elementBox.left - svgBox.left + elementBox.width
        }
        return box
    }


    static getDerivedStateFromProps(nextProps, prevState) {

        if (prevState.paths !== nextProps.paths && nextProps.paths !== null) {
            return { paths: nextProps.paths }
        } else {
            return null
        }
    }


    renderSelectedBoundingbox = (element, mode = "selected") => {
        if(!element) return
        let box2 = null
        let stroke = null, addPixel = null, cursor = null, strokeWidth = null
        if (mode === "selected") {
            stroke = 'rgb(255,0,0,0.8)'
            addPixel = 0
            cursor = 'pointer'
            strokeWidth = 4
        } else {
            stroke = 'rgb(255,0,0,0.3)'
            addPixel = 0
            cursor = 'pointer'
            strokeWidth = 6
        }
        if (refSVG.current) {
            box2 = this.convertToBox(refSVG.current.getBoundingClientRect(), element.box)
        }

        const stylePath =
            { stroke, strokeWidth, cursor }

        return (
            box2 && (
                <g>
                    <line x1={box2.left - addPixel} y1={box2.top - addPixel}
                        x2={box2.right + addPixel} y2={box2.top - addPixel}
                        data-bounding={element.state.id}
                        style={stylePath} />
                    <line x1={box2.left - addPixel} y1={box2.bottom + addPixel}
                        x2={box2.right + addPixel} y2={box2.bottom + addPixel}
                        data-bounding={element.state.id}
                        style={stylePath} />
                    <line x1={box2.left - addPixel} y1={box2.bottom + addPixel}
                        x2={box2.left - addPixel} y2={box2.top - addPixel}
                        data-bounding={element.state.id}
                        style={stylePath} />
                    <line x1={box2.right + addPixel} y1={box2.bottom + addPixel}
                        x2={box2.right + addPixel} y2={box2.top - addPixel}
                        data-bounding={element.state.id}
                        style={stylePath} />
                </g>
            )
        )
    }


    onMouseMoveHandle = async (e) => {
        e.persist()
        let movedElement = e.target.closest('[data-element]')

        let dataMovedOverElement = null
        if (movedElement && e.target.tagName !== 'svg') {
            dataMovedOverElement = movedElement.getAttribute('data-element')
        } else if (e.target.getAttribute('data-bounding')) {
            dataMovedOverElement = e.target.getAttribute('data-bounding')
        }
        const { pathStart, isCreatingPath, onMouseDown, editPathStart, pathElement } = this.state

        if (pathStart && onMouseDown && isCreatingPath) {
            this.updateEndPointOfPath(e)
        }

        if (editPathStart) {
            this.updateStartPointOfPath(e, pathElement)

        } else {
            this.prevX = e.clientX;
            this.prevY = e.clientY;
        }
        this.setState({ dataMovedOverElement })


    }


    onMouseDownHandle = (e) => {
        const boundingOfElement = e.target.getAttribute('data-bounding')
        const path = e.target.getAttribute('data-path')

        const { x, y } = {
            x: e.clientX - refSVG.current.getBoundingClientRect().left,
            y: e.clientY - refSVG.current.getBoundingClientRect().top
        }

        if (boundingOfElement) {
            this.startCreatePath(x, y, boundingOfElement)
        } else if (path) {
            this.selectPath(e, path)
        }
    }

    onMouseUpHandle = (e) => {
        this.finishCreatePath(e)
        window.state = this
    }

    render() {
        const { dataMovedOverElement, isCreatingPath, pathEnd, pathStart } = this.state



        return (
            <svg id='edit' ref={refSVG} xmlns="http://www.w3.org/2000/svg"
                onMouseMove={this.onMouseMoveHandle}
                onMouseDown={this.onMouseDownHandle}
                onMouseUp={this.onMouseUpHandle}
                {...this.props} preserveAspectRatio="xMidYMid meet">

                {isCreatingPath && pathStart && pathEnd && (
                    <path d={`M ${pathStart.x} ${pathStart.y} 
                        q 0 0 ${pathEnd.x - pathStart.x} ${pathEnd.y - pathStart.y}`}
                        style={{ pointerEvents: 'none' }}
                        stroke="blue" strokeWidth="5" fill="none" />
                )}

                {dataMovedOverElement && (
                    <React.Fragment>
                        {this.renderSelectedBoundingbox(ElementContainer.get(dataMovedOverElement), 'move')}
                    </React.Fragment>
                )}

                {renderPath(this.props.paths)}



                <SubscribeOne to={workspaceContainer} bind={['selected']} >
                    {
                        ws => {
                            if (ws.state.selected) {
                                let elementContainer = ElementContainer.get(ws.state.selected)
                                if (!elementContainer) {
                                    elementContainer = PathContainer.get(ws.state.selected)
                                }
                                return (
                                    <SubscribeOne to={elementContainer} bind={['x', 'y']}>
                                        {element => {
                                            return (
                                                <React.Fragment>
                                                    {this.renderSelectedBoundingbox(element)}
                                                    {this.props.children}
                                                </React.Fragment>)
                                        }}
                                    </SubscribeOne>
                                )
                            } else {
                                return <React.Fragment>
                                    {this.props.children}
                                </React.Fragment>
                            }
                        }
                    }
                </SubscribeOne>
            </svg>
        )

    }
}

export default PathBox


