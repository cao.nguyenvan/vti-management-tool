import React from 'react'
import templateContainer, { TemplateContainer } from '../../containers/TemplateContainer'
import workspaceContainer, { REGIONS } from '../../containers/WorkspaceContainer'
import { readJsonFileAsText } from '../../helpers/utils'
import { SubscribeOne } from 'unstated-x'
import { Link } from 'react-router-dom'



const styleMenubar = {
    margin: '0',
    padding: '5px',
    backgroundColor: 'blue'

}

const styleBtnMenubar = {
    marginRight: '10px'
}
class Menubar extends React.Component {

    state = {
        isToggled: false
    }

    handleImportUpload = (e) => {
        const file = e.target.files[0]
        readJsonFileAsText(file).then(result => {
            window.template = result

            if (result) {
                TemplateContainer.importTemplate(result)
            }
        })
    }




    render() {
        return (
            <SubscribeOne to={workspaceContainer} bind={['stack', 'mode']}>
                {
                    ws => {
                        return (
                            <header id="header" className="header">
                                <div className="top-left">
                                    <div className="navbar-header">
                                        <a href="#!" className="navbar-brand">VTI Tool</a>
                                        <a href="#!" className="navbar-brand hidden"><img src="images/logo2.png" alt="Logo" /></a>
                                        <a href="#!" id="menuToggle" className="menutoggle"><i className="fa fa-bars"></i></a>
                                    </div>
                                </div>
                                <div className="top-right">
                                    <div className="header-menu">
                                        <div class="header-left">
                                            <div className="dropdown for-notification">
                                                <button className="btn btn-primary"
                                                    type="button" id="notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Import && Export
                                                </button>
                                                <div className="dropdown-menu" aria-labelledby="notification">
                                                    <a className="dropdown-item media" onClick={() => {
                                                        TemplateContainer.importTemplate()
                                                    }}
                                                        href="#!">
                                                        <i className="fa fa-check"></i>
                                                        <p>Import </p>
                                                    </a>
                                                    <a className="dropdown-item media" onClick={() => {
                                                        TemplateContainer.exportTemplate()
                                                    }} href="#!">
                                                        <i className="fa fa-info"></i>
                                                        <p>Export </p>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </header>
                        )
                    }
                }
            </SubscribeOne>

        )
    }
}

export default Menubar;