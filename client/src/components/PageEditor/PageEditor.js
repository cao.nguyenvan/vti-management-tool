import React from 'react';
import ElementContainer, { PathContainer } from '../../containers/ElementContainer'
import workspaceContainer, { refSVG } from '../../containers/WorkspaceContainer'
import templateContainer from '../../containers/TemplateContainer'
import { SubscribeOne } from 'unstated-x'
import PathBox from './PathBox'
import ToolBox from './ToolBox'
import renderElement from '../../helpers/renderElement'


const ERROR_VALUE_X = 40
const ERROR_VALUE_Y = 33
class PageEditor extends React.Component {


    state = {
        resources: [],
        selectElement: null,
        paths: [],
        isMovingElement: false
    }
    findIntersection = (x1, y1, x2, y2, value, mode = 'x') => {
        // pt duong thang : (y1-y2)*(x-x1) - (x1-x2)*(y-y1) = 0 
        // let intersection = {}
        if (mode === 'x') {
            return {
                x: value,
                y: (y1 - y2) * (value - x1) / (x1 - x2) + y1
            }
        } else {
            return {
                x: (x1 - x2) * (value - y1) / (y1 - y2) + x1,
                y: value
            }
        }
    }

    findIntersectionLineWithBoundingBox = (
        topFrom, leftFrom, rightFrom, bottomFrom,
        x1, y1, x2, y2,
        topTo, leftTo, rightTo, bottomTo) => {
        const intersectionTop = this.findIntersection(x1, y1, x2, y2, topFrom, 'y')
        if (intersectionTop.x <= rightFrom && intersectionTop.x >= leftFrom && y1 >= y2) {
            return {
                intersectionFrom: intersectionTop,
                intersectionTo: this.findIntersection(x1, y1, x2, y2, bottomTo, 'y')
            }
        }

        const intersectionBottom = this.findIntersection(x1, y1, x2, y2, bottomFrom, 'y')
        if (intersectionBottom.x <= rightFrom && intersectionBottom.x >= leftFrom && y1 <= y2) {
            return {
                intersectionFrom: intersectionBottom,
                intersectionTo: this.findIntersection(x1, y1, x2, y2, topTo, 'y')
            }
        }

        const intersectionRight = this.findIntersection(x1, y1, x2, y2, rightFrom, 'x')
        if (intersectionRight.y >= topFrom && intersectionRight.y <= bottomFrom && x1 <= x2) {
            return {
                intersectionFrom: intersectionRight,
                intersectionTo: this.findIntersection(x1, y1, x2, y2, leftTo, 'x')
            }

        }

        const intersectionLeft = this.findIntersection(x1, y1, x2, y2, leftFrom, 'x')
        if (intersectionLeft.y >= topFrom && intersectionLeft.y <= bottomFrom && x1 >= x2) {
            return {
                intersectionFrom: intersectionLeft,
                intersectionTo: this.findIntersection(x1, y1, x2, y2, rightTo, 'x')
            }

        }

    }


    convertCoordinate = (fromElement, toElement, fromX, fromY, toX, toY) => {

    }

    updatePathPosition = (id, addX, addY) => {

        const arrayPaths = Array.from(PathContainer.instances)

        // const pathInstances = arrayPaths.map(e => {
        //     if (e[1].state.point1 === id) {
        //         return { 
        //             container: e[1], 
        //             start: true, 

        //          }
        //     } else if (e[1].state.point2 === id) {
        //         return { container: e[1], start: false }
        //     } else {
        //         return null
        //     }
        // }).filter(e => e !== null)

        const pathInstances = arrayPaths.map(e => {
            if (e[1].state.point1 === id || e[1].state.point2 === id) {
                const fromElement = ElementContainer.get(e[1].state.point1)
                const toElement = ElementContainer.get(e[1].state.point2)
                return {
                    container: e[1],
                    fromElement: {
                        x: fromElement.state.x + ERROR_VALUE_X,
                        y: fromElement.state.y + ERROR_VALUE_Y,
                        top: fromElement.dom.getBoundingClientRect().top - refSVG.current.getBoundingClientRect().top,
                        left: fromElement.dom.getBoundingClientRect().left - refSVG.current.getBoundingClientRect().left,
                        right: fromElement.dom.getBoundingClientRect().right - refSVG.current.getBoundingClientRect().left,
                        bottom: fromElement.dom.getBoundingClientRect().bottom - refSVG.current.getBoundingClientRect().top
                    },
                    toElement: {
                        x: toElement.state.x + ERROR_VALUE_X,
                        y: toElement.state.y + ERROR_VALUE_Y,
                        top: toElement.dom.getBoundingClientRect().top - refSVG.current.getBoundingClientRect().top,
                        left: toElement.dom.getBoundingClientRect().left - refSVG.current.getBoundingClientRect().left,
                        right: toElement.dom.getBoundingClientRect().right - refSVG.current.getBoundingClientRect().left,
                        bottom: toElement.dom.getBoundingClientRect().bottom - refSVG.current.getBoundingClientRect().top
                    }

                }
            } else {
                return null
            }
        }).filter(e => e !== null)

        pathInstances.forEach(e => {
            const intersections = this.findIntersectionLineWithBoundingBox(
                e.fromElement.top, e.fromElement.left, e.fromElement.right, e.fromElement.bottom,
                e.fromElement.x, e.fromElement.y, e.toElement.x, e.toElement.y,
                e.toElement.top, e.toElement.left, e.toElement.right, e.toElement.bottom
            )
            e.container.setState({
                start: intersections.intersectionFrom,
                end: intersections.intersectionTo
            })
        })
    }


    onMouseDownHandle = (e) => {
        window.target = e
        if (!e.target) return


        let selected = e.target.closest('[data-element]')
        if (selected) {
            selected = selected.getAttribute('data-element')
        }
        const point = e.target.getAttribute('data-el')
        const path = e.target.getAttribute('data-path')
        if (selected) {
            this.setState({ isMovingElement: selected })
            workspaceContainer.setState({ selected })
        } else if (!selected && !point && !path && e.target.tagName === 'svg') {
            workspaceContainer.setState({ selected: null })
        }
    }



    onMouseMoveHandle = (e) => {
        const id = this.state.isMovingElement
        const { x, y } = {
            x: e.clientX - refSVG.current.getBoundingClientRect().left - ERROR_VALUE_X,
            y: e.clientY - refSVG.current.getBoundingClientRect().top - ERROR_VALUE_Y
        }

        if (id) {
            const elContainer = ElementContainer.get(id)
            elContainer.setState({ x, y })
            const { addX, addY } = { addX: x - elContainer.state.x, addY: y - elContainer.state.y }
            this.updatePathPosition(id, addX, addY)
        }
    }

    onMouseUpHandle = (e) => {
        this.setState({ isMovingElement: false })
    }


    onDragOverCaptureHandle = (e) => {
        e.preventDefault()
    }


    onDropHandle = async (e) => {

        const type = e.dataTransfer.getData("dataResource");
        const { x, y } = { x: e.clientX - refSVG.current.getBoundingClientRect().left, y: e.clientY - refSVG.current.getBoundingClientRect().top }

        if (type) {

            ElementContainer.addElement({ type, x, y })
            templateContainer.setState({ resources: Array.from(ElementContainer.instances) })
        }

        e.preventDefault()
    }


    render() {

        return (
            <div
                id="page-editor"
                onDragOverCapture={this.onDragOverCaptureHandle}
                onDropCapture={this.onDropHandle}
                onMouseDown={this.onMouseDownHandle}
                onMouseMoveCapture={this.onMouseMoveHandle}
                style={{ margin:'0' , padding:'0' }}
                onMouseUpCapture={this.onMouseUpHandle}>

                <SubscribeOne to={templateContainer} bind={['resources', 'paths']}>
                    {
                        tplContainer => (
                            <PathBox {...this.props} paths={tplContainer.state.paths}>
                                {renderElement(tplContainer.state.resources)}
                            </PathBox>
                        )
                    }
                </SubscribeOne>

                <ToolBox></ToolBox>


            </div>
        )
    }
}

export default PageEditor

