import React from 'react'
import PageEditor from './PageEditor/PageEditor'
import Menubar from './PageEditor/Menubar'
import Inspector from './Inspector/Inspector'
import Catalog from './Sidebar/Catalog';


const getDimensionWindow = () => {
    const width = window.outerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth;
    const height = window.outerHeight
        || document.documentElement.clientHeight
        || document.body.clientHeight;
    return { width, height: height * 70 / 100 }
}

class Main extends React.Component {

    state = {
        isToggled: false,
        height: getDimensionWindow().height,

    }

    componentDidMount() {

        window.addEventListener('resize', () => {
            this.setState({ height: getDimensionWindow().height })
        });


        // //
        // const { stackname } = this.props.match.params
        // if (stackname) {
        //     workspaceContainer.setState({
        //         stack: stackname,
        //         mode: 'manage'
        //     }, async () => {
        //         templateContainer.setState({ stackName: stackname })
        //         const res = await axios.get(`/gettemplate?stackName=${workspaceContainer.state.stack}`)
        //         const template = JSON.parse(`${res.data.TemplateBody}`)
        //         window.template1 = template
        //         TemplateContainer.importTemplate(template)
        //     })

        // } else {
        //     TemplateContainer.clear()
        //     TemplateContainer.reset()
        // }

    }

    // handleImportUpload = (e) => {
    //     const file = e.target.files[0]
    //     readJsonFileAsText(file).then(result => {
    //         window.template = result
    //         if (result) {
    //             TemplateContainer.importTemplate(result)
    //         }
    //     })
    // }





    render() {
        return (
            <React.Fragment>
                <aside id="left-panel" className="left-panel">
                    <Catalog />
                </aside>

                <div id="right-panel" className="right-panel">

                    <Menubar></Menubar>

                    <div className="content">
                        <div className="animated fadeIn">
                            <div className="orders">
                                <div className="row">
                                    <div className="col-xl-8" style={{ marginLeft: '0', padding:'0', marginRight: '0' }} >
                                        <div className="card">
                                            <div className="card-body"
                                                style={{ padding: '0' }}>
                                                <PageEditor width="100%" height="500px" />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-xl-4">
                                        <div className="row">
                                            <div className="col-lg-6 col-xl-12">
                                                <div className="card">
                                                    <div className="card-body">
                                                        <Inspector />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Main;





{/* <React.Fragment>
<Menubar></Menubar>
<div style={{
    display: 'flex',
    flexDirection: 'row'
}}>
    <div id="sidebar" style={{
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'wrap',
        width: "3%",
        background: '#68a0f9',
        paddingTop: '10px'
    }}>
        <Sidebar></Sidebar>
    </div>
    <div style={{
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'wrap',
        width: "79%",
        borderStyle: 'solid',
        borderTop: '0',
        borderLeft: '0',
        borderColor: 'blue'
    }}>


        <PageEditor className="my-4 w-100" id="myChart"
            width="100%"
            height={this.state.height}
            style={{ backgroundColor: '#f9f9f9' }}
        ></PageEditor>

    </div>
    <div style={{
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'wrap',
        width: "18%"
    }}>
        <Inspector />
    </div>
</div>
</React.Fragment> */}