import React from 'react'
import { POOL_ID } from '../../initFromServer'
import axiosx from '../../helpers/axiosx'

import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import { toast } from 'react-toastify';

export default class EditProject extends React.Component {
    constructor(props) {
        super(props)
        let  { datavalue , ...state } = this.props.project 
        this.state = {
            ...this.props.project,
            owners: [],
            departments: [],
            department: datavalue.split('#')[1]
        }
    }
    async componentDidMount() {
        const res1 = await axiosx.get('/apis/department/getdepartments')
        if (res1.data) {
            this.setState({ departments: res1.data.map(e => e.datavalue.split('#')[1]) })

        }

        const res2 = await axiosx.get(`/apis/user/listusersnormal?userpoolid=${POOL_ID}`)
        if (res2.data) {
            this.setState({ owners: res2.data }, () => console.log('check state', this.state.owners))
        }
    }

    handleDayChange = (day) => {
        console.log('check ', day)
        this.setState({ createdat: day });
    }

    onChangeHandle = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    onEditProjectHandle = async ()=>{
        let { department, departments, owners, createdat, ...state} = this.state
        let params  = {}
        if ( createdat !== this.props.project.createdat){
            createdat = `${createdat.getDay()}/${createdat.getMonth() + 1}/${createdat.getFullYear()}`
            console.log('check ', createdat)
            
        }
        params['createdat'] = createdat
        params['datavalue'] = 'project#' + this.state.department

        
        params = {...params,...state }
        console.log('check params', params)
        const res = await axiosx.post('/apis/project/editproject', params)
        if(res.data){
            toast.success('Edit Successful')
            this.props.closeEdit()
        }
        console.log('check res edit',res)

    }




    render() {
        return (
            <div class="card ">
                <div class="container">
                    <div class="card-body">

                        <div id="pay-invoice">
                            <div class="card-body">
                                <div class="form-group text-center">
                                    <ul class="list-inline">
                                        <li class="list-inline-item"><i
                                            class="text-muted fa fa-cc-visa fa-2x"></i></li>
                                        <li class="list-inline-item"><i
                                            class="fa fa-cc-mastercard fa-2x"></i></li>
                                        <li class="list-inline-item"><i class="fa fa-cc-amex fa-2x"></i>
                                        </li>
                                        <li class="list-inline-item"><i class="fa fa-cc-discover fa-2x"></i>
                                        </li>
                                    </ul>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-1"> Project Name</label>
                                    <input id="cc-payment" name="projectname" type="text"
                                        value={this.state.projectname} placeholder="Type the project name"
                                        onChange={this.onChangeHandle}
                                        class="form-control" aria-required="true"
                                    />
                                </div>
                                <div class="form-group">
                                    <label for="cc-exp"
                                        class="control-label mr-3">Create At</label>
                                    <DayPickerInput
                                        className="form-control"
                                        selectedDay={this.state.createdat} onDayChange={this.handleDayChange} />
                                </div>

                                <div class="form-group has-success">
                                    <label for="cc-name" class="control-label mb-1">Description</label>
                                    <textarea id="cc-name" name="description" type="text"
                                        onChange={this.onChangeHandle} placeholder="Type the description"
                                        value={this.state.description}
                                        class="form-control" />
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="cc-number" class="control-label mb-1">Department</label>
                                            <select name="department"
                                                value={this.state.department}
                                                onChange={this.onChangeHandle}
                                                id="selectSm" class="form-control">
                                                <option value="0">Please select</option>
                                                {
                                                    this.state.departments.map(e => (
                                                        <option key={e} value={e}>{e}</option>
                                                    ))
                                                }
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="x_card_code" class="control-label mb-1">
                                            Owner
                </label>
                                        <select name="ownervalue"
                                            value={this.state.ownervalue}
                                            onChange={this.onChangeHandle}
                                            id="selectSm" class="form-control">
                                            <option value="0">Please select</option>
                                            {
                                                this.state.owners.map(e => (
                                                    <option key={e.sub} value={e.email}>{e.email}</option>
                                                ))
                                            }
                                        </select>
                                    </div>
                                </div>
                                <div>
                                    <button
                                        onClick={this.onEditProjectHandle}
                                        type="submit" class="btn btn-lg btn-info btn-block">
                                        <span>Edit Project</span>
                                    </button>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
        )
    }
}