import React from 'react'
import { Link, withRouter } from 'react-router-dom'
import axiosx from '../../helpers/axiosx'
import { toast } from 'react-toastify';
import {rootPath } from '../../App'

const ICONS = ['room', 'store', 'airplanemode_active']

const getIcon = () => {
    return ICONS[Math.floor(Math.random() * ICONS.length)];
}


class ProjectList extends React.Component {

    state = {
        projects: []
    }

    async componentDidMount() {
        const res = await axiosx.get('/apis/project/getallprojects')
        if (res.data) {
            this.setState({ projects: res.data })
            await window.$('#bootstrap-data-table').DataTable();
        }
    }

    onDeleteProjectHandle = async (project) => {
        if (window.confirm(`Are you sure to delete project ${project.projectname}`)) {
            const res = await axiosx.post('/apis/project/deleteproject', { id: project.pk , ...project })
            if (res.data) {
                this.setState({ projects: this.state.projects.filter(e => project.pk !== e.pk) }, () => {
                    toast.error('Delete successful!')
                })
            }
        }

    }

    render() {
        return (
            <React.Fragment>
                <button
                    onClick={() => this.props.history.push(`${rootPath}/addproject`)}
                    type="button" style={{ borderRadius: '50%' }} className="btn btn-success ml-3 mb-2">
                    <i className="fas fa-plus"></i>
                </button>
                <div style={{ clear: 'both' }}></div>
                <div className="card">
                    <div className="card-body">
                        <table id="bootstrap-data-table" className="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Project Name</th>
                                    <th>Created At</th>
                                    <th>Department</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.projects.map(e => {
                                        return (
                                            <tr key={e.pk}>
                                                <td><Link to={`${rootPath}/projectdetail?projectid=${e.pk.split('#')[1]}`}>{e.projectname}</Link></td>
                                                <td>{e.createdat}</td>
                                                <td>{e.datavalue.split('#')[1]}</td>
                                                <td>
                                                    <i className="fas fa-trash-alt"
                                                        onClick={() => this.onDeleteProjectHandle(e)}
                                                        style={{ cursor: 'pointer' }}></i></td>
                                            </tr>
                                        )
                                    })
                                }

                            </tbody>
                        </table>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default withRouter(ProjectList)