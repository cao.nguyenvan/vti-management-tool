import React from 'react'
import axiosx from '../../helpers/axiosx';
import { POOL_ID } from '../../initFromServer'
import EditProject from './EditProject'

export default class ProjectDetail extends React.Component {
    state = {
        project: null,
        edit: false
    }
    async componentDidMount() {
        await this.getProjectDetail()
    }
    getProjectDetail = async ()=>{
        const urlParams = new URLSearchParams(window.location.search)
        const projectId = urlParams.get('projectid')
        console.log('checkkk ')
        const res = await axiosx.get(`/apis/project/getprojectdetail?projectid=${projectId}`)
        if (res.data) {
            this.setState({ project: res.data }, () => console.log('check ', this.state.project))
        }
    }
    render() {
        // const { description, createdat, datavalue, owner, projectname } = this.state.project
        console.log('check project', this.state.project)
        return (

            this.state.project && (
                <React.Fragment>
                    {
                        this.state.edit ? (
                            <EditProject 
                            closeEdit = {()=> this.setState({edit:false},async ()=>{
                                await this.getProjectDetail()
                            })}
                            project={this.state.project} />
                        ) : (
                                <div className="container">
                                    <h1>
                                        {this.state.project.projectname}
                                    </h1>
                                    <div className="row mt-5">
                                        <div className="col col-md-6">
                                            <div className="row mt-2">
                                                <div className="col col-md-4">
                                                    <strong>Owner:</strong>
                                                </div>
                                                <div className="col col-md-6">
                                                    {this.state.project.ownervalue}
                                                </div>
                                            </div>
                                            <div className="row mt-2">
                                                <div className="col col-md-4">
                                                    <strong>Created At:</strong>
                                                </div>
                                                <div className="col col-md-6">
                                                    {this.state.project.createdat}
                                                </div>
                                            </div>


                                        </div>
                                        <div className="col col-md-5">
                                            <div className="row mt-2">
                                                <div className="col col-md-4">
                                                    <strong>Department:</strong>
                                                </div>
                                                <div className="col col-md-6">
                                                    {this.state.project.datavalue.split('#')[1]}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mt-2">
                                        <div className="col col-md-2">
                                            <strong>Description:</strong>
                                        </div>
                                        <div className="col col-md-6">
                                            <p>{this.state.project.description}</p>
                                        </div>
                                    </div>
                                    <div className="row mt-2">
                                        <button class="btn btn-primary btn-sm" onClick={() => this.setState({ edit: true })}>Edit</button>
                                    </div>
                                    <div className="row mt-2">
                                        <div className="col col-md-1" />
                                        <div className="col col-md-8">
                                            {this.state.project.stages.length > 0 && (
                                                <table class="table">
                                                    <thead class="thead-dark">
                                                        <tr>
                                                            <th scope="col">#</th>
                                                            <th scope="col">Environment</th>
                                                            <th scope="col">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {this.state.project.stages.map((e, index) => {
                                                            return (
                                                                <tr key={e}>
                                                                    <th scope="row">{index + 1}</th>
                                                                    <td>{e}</td>
                                                                    <td>Otto</td>
                                                                </tr>
                                                            )
                                                        })}
                                                    </tbody>
                                                </table>)}
                                        </div>
                                    </div>
                                </div>
                            )
                    }
                </React.Fragment>
            )
        )
    }
}