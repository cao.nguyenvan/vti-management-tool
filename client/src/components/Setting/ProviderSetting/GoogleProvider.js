import React from 'react'
import axiosx from '../../../helpers/axiosx'
import { POOL_ID, APP_CLIENT_ID } from '../../../initFromServer'
import { toast } from 'react-toastify';

export default class GoogleProvider extends React.Component {

    state = {
        isGoogle: this.props.isGoogle,
        googleClientId: this.props.googleClientId,
        googleSecretId: this.props.googleSecretId
    }
    onChangeProviderStatus = async (e) => {
        this.setState({ isGoogle: e.target.checked })
    }

    onChangeHandle = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    onSaveChangeHandle = async (e) => {
        let res = null

        if (this.props.isGoogle !== this.state.isGoogle) {
            if (this.props.isGoogle === false) {
                //// Turn on Gooogle
                res = await axiosx.post('/apis/sso/addgoogleprovider', {
                    poolUserID: POOL_ID,
                    googleClientId: this.state.googleClientId,
                    googleSecretId: this.state.googleSecretId,
                    callbackURL: `${window.location.origin}/dev`,
                    clientId: APP_CLIENT_ID
                })
                await toast.success('add successful')
            } else {
                ///  Turn of Gooogle 
                res = await axiosx.post('/apis/sso/removegoogleprovider', {
                    poolUserID: POOL_ID,
                    callbackURL: `${window.location.origin}/dev`,
                    clientId: APP_CLIENT_ID
                })
                await toast.warn('delete successful')
            }
        } else {
            res = await axiosx.post('/apis/sso/updategoogleprovider', {
                poolUserID: POOL_ID,
                googleClientId: this.state.googleClientId,
                googleSecretId: this.state.googleSecretId,
                callbackURL: `${window.location.origin}/dev`,
                clientId: APP_CLIENT_ID
            })
            await toast.success('update successful')
        }
        await this.props.setState('isGoogle', this.state.isGoogle)
        const elems = document.querySelectorAll('.modal');

        const instance = window.M.Modal.getInstance(elems);
        await instance.close()


    }
    render() {
        const { googleClientId, googleSecretId, isGoogle } = this.state
        return (
            <div class="card">
                <div class="card-header"><strong>Google Provider Settings</strong></div>


                <div class="card-body card-block">

                    <div class="form-check">
                        <div class="checkbox">
                            <label for="checkbox1" class="form-check-label ">
                                <input type="checkbox" id="checkbox1" name="checkbox1"
                                    checked={this.state.isGoogle} onChange={this.onChangeProviderStatus}
                                    value="option1" class="form-check-input" />{isGoogle ? 'Enable' : 'Disable'}
                            </label>
                        </div>
                    </div>
                    {
                        isGoogle && (
                            <React.Fragment>
                                <div class="form-group">
                                    <label for="company" class=" form-control-label">Google Client Id</label>
                                    <input type="text" name='googleClientId'
                                        onChange={this.onChangeHandle}
                                        value={googleClientId}
                                        placeholder="google client id" class="form-control" />
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">Google Secret Key</label>
                                    <input type="text" name='googleSecretId'
                                        onChange={this.onChangeHandle}
                                        value={googleSecretId}
                                        placeholder="google secret key" class="form-control" />
                                </div>
                            </React.Fragment>
                        )
                    }


                    <button className="btn " onClick={this.onSaveChangeHandle} >Save Changes</button>

                </div>
            </div>
        )
    }
}




