import React from 'react'
import axiosx from '../../helpers/axiosx'
import { POOL_ID } from '../../initFromServer'
import { toast } from 'react-toastify'

export default class User extends React.Component {

    state = {
        users: [],
        addUser: false,
        emailNewUser: '',
        passwordNewUser: '',
        admin:false
    }

    componentDidMount = async () => {

        const res = await axiosx.get(`/apis/user/listusers?userpoolid=${POOL_ID}`)
        window.res = res.data
        // await this.setState({
        //     users: e.log('chech users',this.state.users))
        const users = res.data
        await this.setState({ users })
        await window.$('#bootstrap-data-table').DataTable();

    }

    onDeleteUser = async (user) => {

        if (window.confirm(`Are you sure to delete ${user.email}`)) {
            const res = await axiosx.post(`/apis/user/deleteuser`, {
                userName: user.username,
                userPoolId: POOL_ID
            })
            if (res.data) {
                await toast.success('Delete successful !')
                await this.setState((prevState, props) => {
                    return {
                        users: prevState.users.filter(e => e.sub !== user.sub)
                    }
                })
            }

        }
    }

    onChangeAdminStatus = async (user, target) => {
        window.target = target
        const valueCheck = target.checked
        if (target.checked) {
            const res = await axiosx.post(`/apis/user/admingroup`, {
                add: true,
                userPoolId: POOL_ID,
                userName: user.username
            })
            await toast.success(`Add ${user.email} to admin group`)
        } else {
            const res = await axiosx.post(`/apis/user/admingroup`, {
                add: false,
                userPoolId: POOL_ID,
                userName: user.username
            })
            await toast.error(`Remove ${user.email} to admin group`)
        }
        target.checked = valueCheck
    }


    onChangeHandle = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    onAddNewUser = async (e)=>{
        const res = await axiosx.post('/apis/user/adduser',{
            userName: this.state.emailNewUser,
            password: this.state.passwordNewUser,
            userPoolId: POOL_ID,
            admin : this.state.admin
        })

        if(res.data){
            let users = this.state.users
            const user = {...res.data,admin:this.state.admin}
            users.push(user)
            this.setState({users , addUser:false},()=>{
                toast.success('Add User successfull')
            })
        }
       
    }

    render() {
        return (
            <React.Fragment>
                {
                    this.state.addUser ? (
                        <React.Fragment>
                            <div class="row">

                                <div class="col-md-5 form">
                                    <div class="form-group">
                                        <label class=" form-control-label">Email </label>
                                        <input type="text" name='emailNewUser'
                                            onChange={this.onChangeHandle}
                                            value={this.state.emailNewUser}
                                            placeholder="Enter the email" class="form-control" />
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Password</label>
                                        <input type="text" name='passwordNewUser'
                                            onChange={this.onChangeHandle}
                                            value={this.state.passwordNewUser}
                                            placeholder="Enter the password" class="form-control" />
                                    </div>

                                    <div class="form-check">
                                        <div class="checkbox">
                                            <label class="form-check-label ">
                                                <input type="checkbox"
                                                    checked={this.state.admin} onChange={(e) => this.setState({ admin: e.target.checked })}
                                                    class="form-check-input" /> Is Admin 
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <button class="btn btn-success mb-3 ml-3 mr-5"
                                    onClick={this.onAddNewUser}
                                > Add User </button>
                                <button class="btn btn-primary mb-3"
                                    onClick={() => this.setState({ addUser: false })}
                                > Close </button>
                            </div>
                        </React.Fragment>

                    ) : (
                            <button class="btn btn-success mb-3"
                                onClick={() => this.setState({ addUser: true })}
                            > Add User </button>
                        )
                }


                <div class="row">

                    <div class="col-md-12">


                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Users </strong>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Sub</th>
                                            <th>Email</th>
                                            <th>Actions</th>
                                            <th>Amin</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.users.map(e => {
                                                return (
                                                    <tr key={e.sub}>
                                                        <td>{e.sub}</td>
                                                        <td>{e.email}</td>
                                                        <td><i
                                                            onClick={() => this.onDeleteUser(e)}
                                                            style={{ cursor: 'pointer' }}
                                                            class="fas fa-trash-alt"></i></td>
                                                        <td><input
                                                            onChange={(event) => this.onChangeAdminStatus(e, event.target)}
                                                            checked={e.admin}
                                                            type="checkbox" /></td>
                                                    </tr>
                                                )
                                            })
                                        }

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}