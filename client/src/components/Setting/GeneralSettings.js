import React from 'react'
import AccessControl from './AccessControl'
import Department from './Department'

export default class GeneralSettings extends React.Component {

    render() {
        return (
            <div id="accordion">
                <div className="card">
                    <div className="card-header" data-toggle="collapse" data-target="#collapseOne"
                        aria-expanded="true" aria-controls="collapseOne" id="headingOne">
                        <div className="mb-0">
                            <h5> Department </h5>
                            <p>Manage departments in company</p>
                        </div>
                    </div>

                    <div id="collapseOne" className="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div className="card-body">
                            <Department/>
                        </div>
                    </div>
                </div>
                <div className="card">
                    <div className="card-header " id="headingTwo">
                        <div className="mb-0 active" data-toggle="collapse" data-target="#collapseTwo"
                            aria-expanded="true" aria-controls="collapseTwo" id="headingTwo">
                            <h5>Single Sign On (SSO)</h5>
                            <p>Allow single sign on (SSO) identify providers</p>
                        </div>
                    </div>
                    <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div className="card-body">
                            <AccessControl></AccessControl>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}




{/* <ul className="collapsible">
<li>
    <div className="collapsible-header" style={styleCollapseHeader}>
        <div>
            <h5>Department</h5>
            <p>Manage departments in company</p>
            <div className="divider s12"></div>
        </div>

    </div>
    <div className="collapsible-body">
        <div>
    
        </div>
    </div>
</li>
<li className="active">
    <div className="collapsible-header"  style={styleCollapseHeader}>
        <div>
            <h5>Single Sign On (SSO)</h5>
            <p>Allow single sign on (SSO) identify providers</p>
            <div className="divider s12"></div>
        </div>
    </div>
    <div className="collapsible-body">
        <AccessControl></AccessControl>
    </div>
</li>
</ul> */}