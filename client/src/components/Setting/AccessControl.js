import React from 'react'
import axiosx from  '../../helpers/axiosx'
import GoogleProvider from './ProviderSetting/GoogleProvider'

import { toast } from 'react-toastify'

class AccessControl extends React.Component {


    state = {
        domains: [],
        isGoogle: false,
        isFacebook: false,
        isAmazon: false,
        googleClientId: '',
        googleSecretId: '',
        facebookClientId: '',
        facebookSecretId: '',
        amazonClientId: '',
        amazonSecretId: '',
        addDomain: '',
        mode: null
    }

    componentDidMount = async () => {

        // fetch data
        const response = await axiosx.get('/apis/sso/getssosetting')
        window.response = response.data
        if (response.data) {
            const domainItems = response.data.find(e => e.domainvalue)
            if (domainItems) {
                this.setState({ domains: domainItems.domainvalue.split(',') })
            }

            const googleItem = response.data.find(e => e.sk === 'google')
            if (googleItem) {
                this.setState({
                    isGoogle: true,
                    googleClientId: googleItem.googleClientId,
                    googleSecretId: googleItem.googleSecretId
                })
            }

        }
    }

    onChangeHandle = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    onChangeProviderStatusHandle = (e) => {
        this.setState({ [e.target.name]: e.target.checked })
    }

    onMinusDomainHandle = (value) => {
        this.setState((prevState, props) => {
            return {
                domains: prevState.domains.filter(e => e !== value)
            }
        })
    }

    onAddDomainHandle = () => {
        window.thisx = this
        let domains = this.state.domains
        if (!this.state.domains.find(e => e === this.state.addDomain)) {
            domains.push(this.state.addDomain)
            this.setState({ domains, addDomain: '' })
        }
    }

    onSaveChangeHandle = async () => {
        const domains = this.state.domains.join(',')
        if (this.state.domains.length === 0) {
            await axiosx.post('/apis/sso/removewhitelistdomain',
                { domain: this.state.domains.join(',') })
        } else {
            await axiosx.post('/apis/sso/updatewhitelistdomain',
                { domain: this.state.domains.join(',') })
        }

        await toast.success('Save Whitelist successfully ')
    }

    renderProvider = () => {
        switch (this.state.mode) {
            case 'google':
                return <GoogleProvider
                    isGoogle={this.state.isGoogle}
                    googleClientId={this.state.googleClientId}
                    googleSecretId={this.state.googleSecretId}
                    setState={(property, value) => this.setState({ [property]: value })}
                />
            default:
                return null
        }
    }



    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-lg-4 col-md-6">
                        <div className="card">
                            <div className="card-body">
                                <div className="stat-widget-five" onClick={() => {
                                    if (this.state.mode === 'google') {
                                        this.setState({ mode: null })
                                    } else {
                                        this.setState({ mode: 'google' })
                                    }

                                }}>
                                    <div className="stat-icon dib flat-color-1">
                                        <i className="ti-google"></i>
                                    </div>
                                    <div className="stat-content">
                                        <div className="text-left dib">
                                            <div className="stat-text"><span>Google</span></div>
                                            <div className="stat-heading">Revenue</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-4 col-md-6">
                        <div className="card">
                            <div className="card-body">
                                <div className="stat-widget-five">
                                    <div className="stat-icon dib flat-color-2">
                                        <i className="ti-facebook"></i>
                                    </div>
                                    <div className="stat-content">
                                        <div className="text-left dib">
                                            <div className="stat-text"><span>Facebook</span></div>
                                            <div className="stat-heading">Sales</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-4 col-md-6">
                        <div className="card">
                            <div className="card-body">
                                <div className="stat-widget-five">
                                    <div className="stat-icon dib flat-color-3">
                                        <i className="ti-amazon"></i>
                                    </div>
                                    <div className="stat-content">
                                        <div className="text-left dib">
                                            <div className="stat-text"><span>Amazon</span></div>
                                            <div className="stat-heading">Templates</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        {this.renderProvider()}
                    </div>


                </div>
                <div className="row">

                    <label className="col-lg-5 col-md-6">White List Domain</label>
                </div>
                <div className="row">
                    <div className="col-lg-5 col-md-6 ml-1 mb-1">

                        {this.state.domains.map(e => (
                            <span key={e} className="badge badge-primary mr-1">{e}
                                <i className="fas fa-times ml-1"
                                    onClick={() => this.onMinusDomainHandle(e)}
                                ></i></span>
                        ))}
                    </div>
                </div>
                <div className="row">

                    <div className="col-lg-4 col-md-6">
                        <div className="input-group">
                            <input type="text"
                                name='addDomain'
                                onChange={this.onChangeHandle}
                                value={this.state.addDomain}
                                placeholder="Domain " className="form-control" />

                            <div className="input-group-btn"><button
                                onClick={this.onAddDomainHandle}
                                className="btn btn-primary">
                                <i className="fas fa-plus"></i></button></div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-4 col-md-6 mt-3">
                        <button className="btn btn-success"
                            onClick={this.onSaveChangeHandle}
                        >Save Changes White List</button>
                    </div>

                </div>
            </div>

        )
    }
}


export default AccessControl




