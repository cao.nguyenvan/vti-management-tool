import React from 'react'
import axiosx from '../../helpers/axiosx'
import uuid from 'uuid'
import { toast, ToastPosition } from 'react-toastify'

export default class Department extends React.Component {
    state = {
        departments: [],
        edit: null,
        editValue: null,
        departmentName: ''
    }
    async componentDidMount() {
        const res = await axiosx.get('/apis/department/getdepartments')
        if (res.data) {
            this.setState({ departments: res.data })
        }
        window.departments = res.data

    }
    onChangeHandle = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    onSaveEditChangeHandle = async () => {
        if (this.state.edit.datavalue !== 'department#' + this.state.editValue.split(' ').join('')) {
            const res = await axiosx.post('/apis/department/updatedepartment', {
                id: this.state.edit.pk,
                name: this.state.editValue.split(' ').join(''),
                oldName: this.state.edit.datavalue.split('#')[1]
            })
            const params = {
                id: this.state.edit.pk,
                name: this.state.editValue.split(' ').join(''),
                oldName: this.state.edit.datavalue
            }
            console.log('check ', params)
            if (res.data) {
                let departments = this.state.departments
                for (let i = 0; i < this.state.departments.length; i++) {
                    if (this.state.departments[i].pk === this.state.edit.pk) {
                        departments[i].datavalue = 'department#' + this.state.editValue
                        break
                    }
                }
                this.setState({ departments, edit: null, editValue: null }, () => toast.success('Edit successful'))
            }
        } else {
            this.setState({ edit: null, editValue: null })
        }
    }

    onAddHandle = async () => {
        if (this.state.departments.find(e => e.datavalue === 'department#' + this.state.departmentName)) {
            window.departments = this.state.departments
            alert('Department is existed !')
            return
        }
        if (this.state.departmentName.split(' ').join('') !== '') {
            const id = uuid()
            const res = await axiosx.post('/apis/department/adddepartment', {
                id,
                name: this.state.departmentName
            })

            if (res.data) {
                let departments = this.state.departments
                departments.push({
                    pk: 'department#' + id, sk: 'DEPARTMENT', datavalue: 'department#' + this.state.departmentName
                })
                this.setState({ departments, departmentName: '' }, () => toast.success('Add department Successful'))
            }
        }
    }

    onDeleteHandle = async (department) => {
        if (window.confirm('Are you sure to delete this department ?')) {
            console.log('checkkkkkk delete')
            const res = await axiosx.post('/apis/department/deletedepartment', {
                id: department.pk,
                name: department.datavalue.split('#')[1]
            })
            console.log('check res',res)

            if (res.data) {
                if(res.data.warning){
                    alert(res.data.warning)
                    return
                }
                let departments = this.state.departments.filter(e => e.pk !== department.pk)
                this.setState({ departments }, () => toast.error('Delete done !'))
            }
        }


    }
    render() {
        return (

            <div className="card">
                {/* <div className="card-header blue">
                    <strong className="card-title">Bordered Table</strong>
                </div> */}
                <div className="card-body">
                    <table className="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.departments.map((e, i) => (
                                    <tr key={e.pk}>
                                        <th style={{ width: '10%' }} scope="row">{i + 1}</th>

                                        {
                                            this.state.edit && this.state.edit.pk === e.pk ? (
                                                <td style={{ width: '50%' }} scope="row"><input value={this.state.editValue}
                                                    name="editValue" className="form-control"
                                                    onChange={this.onChangeHandle} /></td>
                                            ) : (
                                                    <td style={{ width: '50%' }} scope="row">{e.datavalue.split('#')[1]} </td>
                                                )
                                        }

                                        <td scope="row" >

                                            < i className="fas fa-edit mr-4" style={{ cursor: 'pointer' }}
                                                onClick={() => {
                                                    if (!this.state.edit) {
                                                        this.setState({ edit: e, editValue: e.datavalue.split('#')[1] })
                                                    } else {
                                                        this.setState({ edit: null, editValue: null })
                                                    }
                                                }}
                                            ></i>
                                            <i className="fas fa-trash-alt mr-4"
                                                onClick={() => this.onDeleteHandle(e)}
                                                style={{ cursor: 'pointer' }}></i>
                                            {
                                                (this.state.edit && this.state.edit.pk === e.pk) && (
                                                    <button
                                                        onClick={this.onSaveEditChangeHandle}
                                                        class="btn btn-sm btn-primary">Save</button>
                                                )}
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>
                    <div className="form-group ">

                        <input type="text" className="form-control"
                            name="departmentName" value={this.state.departmentName}
                            onChange={this.onChangeHandle}
                            placeholder="New Department" />
                        <button
                            onClick={this.onAddHandle}
                            className="btn btn-success mt-2">Save Changes</button>

                    </div>
                </div>

            </div>
        )
    }
}