import React from 'react'
import { ComponentHierachy } from '../../App'


import { Link } from 'react-router-dom'

export default class Sidenav extends React.Component {
    state = {
        mode: 'generalsetting',
        isChoosed: 1
    }

    onChangeModeHandle = (mode) => {
        this.setState({
            mode
        })
    }

    renderSideNavItem = () => {
        return (
            ComponentHierachy.filter(e => !e.Parent).map((parent, i) => (
                <React.Fragment key={parent.Name}  >
                    <li className="menu-title" >
                        {parent.Path ? (
                            <Link to={parent.Path}>{parent.Name}</Link>
                        ) : (
                                <React.Fragment>{parent.Name}</React.Fragment>
                            )}

                    </li>

                    {
                        parent.Children && (
                            <React.Fragment>
                                {
                                    parent.Children.map(e => {
                                        const { Name, Path } = ComponentHierachy[e]
                                        return (
                                            <li key={Name} className="menu-item-has-children dropdown">
                                                {
                                                    Path ? (

                                                        <Link to={Path} className="dropdown-toggle"> <i className="menu-icon fa fa-cogs"></i>{Name}</Link>
                                                    ) : (
                                                            <a href="#!" className="dropdown-toggle"> <i className="menu-icon fa fa-cogs"></i>{Name}</a>
                                                        )
                                                }
                                            </li>
                                        )
                                    })
                                }
                            </React.Fragment>
                        )
                    }
                </React.Fragment>
            ))
        )
    }
    render() {
        return (
            <aside id="left-panel" className="left-panel">
                <nav className="navbar navbar-expand-sm navbar-default">
                    <div id="main-menu" className="main-menu collapse navbar-collapse">
                        <ul className="nav navbar-nav">
                            <li>
                                <a href="index.html"><i className="menu-icon fa fa-laptop"></i>Dashboard </a>
                            </li>
                            {this.renderSideNavItem()}
                        </ul>
                    </div>
                </nav>
            </aside>

        )
    }
}


