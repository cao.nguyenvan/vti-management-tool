import React from 'react'
import Elements from '../../elements/Elements'
import ElementContainer, { BindingContext } from '../../containers/ElementContainer'
import workspaceContainer from '../../containers/WorkspaceContainer'
import { SubscribeOne } from 'unstated-x'



class Inspector extends React.Component {
    generateInspector = (selected) => {
        if (selected) {
            const element = ElementContainer.get(selected)
            window.selected = element

            if (element) {
                const Content = Elements[element.state.type].generateInspector
                return (
                    <BindingContext.Provider value={element.state.data}>

                        <Content></Content>

                    </BindingContext.Provider>

                )
            } else {
                return null
            }

        }
    }
    render() {

        return (
            <div>
                <SubscribeOne to={workspaceContainer} bind={['selected']}>
                    {ws => (
                        <ul className="nav">
                            {this.generateInspector(ws.state.selected)}
                        </ul>
                    )}
                </SubscribeOne>
            </div>
        )

    }
}

export default Inspector