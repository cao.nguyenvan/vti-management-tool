import React from 'react'
import { BindingContext } from '../../../containers/ElementContainer'
import { SubscribeOne, Subscribe } from 'unstated-x'


const styleInput = {
    margin: '10px',
    display: 'flex',
    justifyContent: 'space-between'
}

const styleTextInput = {
    width: '70%',
    height: '25px',
    borderRadius: '5px',
    borderWidth: '1px',
    padding: '0',
    resize: 'vertical'
}

export default class BindTextInput extends React.Component {

    render() {

        const bindProperty = this.props.bind.split('.')[1]
        const { title, placeholder, type } = this.props

        return (
            <BindingContext.Consumer>
                {dataContainer => (
                    <Subscribe to={[dataContainer]}>
                        {
                            data => (
                                <div style={styleInput}>
                                    <label className="inline">{title}</label>
                                    {
                                        type === 'textarea' ? (
                                            <textarea
                                                style={styleTextInput}
                                                type="text" className="browser-default"
                                                placeholder={placeholder ? placeholder : ''}
                                                onChange={(e) => data.setState({ [bindProperty]: e.target.value })}
                                                value={data.state[bindProperty]}
                                                {...this.props}
                                            />
                                        ) : (
                                                <input
                                                    style={styleTextInput}
                                                    type="text" className="browser-default"
                                                    placeholder={placeholder ? placeholder : ''}
                                                    onChange={(e) => data.setState({ [bindProperty]: e.target.value })}
                                                    value={data.state[bindProperty]}
                                                    {...this.props}
                                                />
                                            )
                                    }

                                </div>
                            )
                        }
                    </Subscribe>
                )}
            </BindingContext.Consumer>
        )
    }
}