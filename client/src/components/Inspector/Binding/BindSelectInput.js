import React from 'react'
import { BindingContext } from '../../../containers/ElementContainer'
import { SubscribeOne, Subscribe } from 'unstated-x'


const styleInput = {
    margin: '10px',
    display: 'flex',
    justifyContent: 'space-between'
}

const styleTextInput = {
    width: '70%',
    height: '25px',
    borderRadius: '5px',
    borderWidth: '1px',
    padding: '0'
}

export default class BindSelectInput extends React.Component {

    render() {

        const bindProperty = this.props.bind.split('.')[1]
        const { title, placeholder , listItem } = this.props
        return (
            <BindingContext.Consumer>
                {dataContainer => (
                    <Subscribe to={[dataContainer]}>
                        {
                            data => (
                                <div style={styleInput}>
                                    <label >{title}</label>
                                    <select className="browser-default" style={styleTextInput}
                                        value= {data.state[bindProperty]}
                                        onChange={(e) => data.setState({[bindProperty]:e.target.value})}
                                    >
                                        <React.Fragment>
                                            {
                                               listItem.map(e=>(
                                                   <option key={e} value={e}>{e}</option>
                                               ))
                                            }
                                        </React.Fragment>
                                    </select>
                                </div>
                            )
                        }
                    </Subscribe>
                )}
            </BindingContext.Consumer>
        )
    }
}