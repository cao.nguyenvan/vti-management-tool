import React from 'react'
import { BindingContext } from '../../../containers/ElementContainer'
import { SubscribeOne, Subscribe } from 'unstated-x'


const styleInput = {
    margin: '10px',
    display: 'flex',
    justifyContent: 'space-between'
}

// const styleTextInput = {
//     width: '70%',
//     height: '25px',
//     borderRadius: '5px',
//     borderWidth: '1px',
//     padding: '0'
// }

const styleTag = {
    width: '30%',
    height: '25px',
    borderRadius: '5px',
    borderWidth: '1px',
    padding: '0'
}

const styleTagsBody = {
    margin: '0',
    display: 'flex',
    marginLeft: '20%',
    marginRight: '20%',
    justifyContent: 'space-between'
}


export default class BindTag extends React.Component {

    state = {
        key: '',
        value: ''
    }

    onChangeHandle = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    render() {
        return (
            <BindingContext.Consumer>
                {dataContainer => (
                    <Subscribe to={[dataContainer]}>
                        {
                            data => (
                                <React.Fragment>
                                    <div style={styleInput}>
                                        <label>Tags</label>
                                        <input
                                            style={styleTag} placeholder="key..."
                                            onChange={this.onChangeHandle}
                                            name="key"
                                            type="text" className="browser-default" />
                                        <input
                                            style={styleTag} placeholder="value..."
                                            onChange={this.onChangeHandle}
                                            name="value"
                                            id="last_name" type="text" className="browser-default" />
                                        <a href="#!" className="btn-floating btn-small waves-effect waves-light blue lighten-3"
                                            onClick={() => {
                                                let tags = data.state.Tags
                                                tags.push({ Key: this.state.key, Value: this.state.value })
                                                data.setState({ Tags: tags },()=>{
                                                    this.setState({value:'',key:''})
                                                })
                                            }}>
                                            <i className="material-icons">add</i></a>
                                    </div>
                                    <React.Fragment>
                                        {
                                            data.state.Tags.map(e => (
                                                <div key={e.Key} style={styleTagsBody}>
                                                    <h7 ><strong>{e.Key}</strong></h7>
                                                    <h7>{e.Value}</h7>
                                                    <i 
                                                    onClick={()=>{
                                                        let tags = data.state.Tags
                                                        data.setState({ Tags: tags.filter(checkEl => checkEl.Key !== e.Key) })
                                                    }}
                                                    className="material-icons red-text darken-3" >
                                                        indeterminate_check_box</i>
                                                </div>
                                            ))

                                        }
                                    </React.Fragment>

                                </React.Fragment>

                            )
                        }
                    </Subscribe>
                )}
            </BindingContext.Consumer>
        )
    }
}