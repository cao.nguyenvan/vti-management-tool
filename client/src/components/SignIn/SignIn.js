import React from "react";
import { Link } from "react-router-dom";
import { URL_TO_GOOGLE } from "../../config";
import { Auth } from "aws-amplify";
// import "./SignIn.css";

class SignIn extends React.Component {
  constructor() {
    super();
    this.state = {
      username: "",
      password: "",
      showNewPasswordRequire: false,
      newPassword: "",
      reNewPassword: "",
      user: null
    };

  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    const user = await Auth.signIn(this.state.username, this.state.password);
    await this.setState({ user: user });


    if (user.challengeName === "NEW_PASSWORD_REQUIRED") {
      // display new password screen
      this.setState({ showNewPasswordRequire: true });
    } else {
      // TODO anh.nguyentientuan redirect to home page
      await this.props.history.push(window.location.href.indexOf('/dev') !== -1 ? '/dev' : '/')
    }
  }

  onSignInWithGoogle = () => {
    window.location.assign(URL_TO_GOOGLE);
  }

  handleSubmitNewPassword = async (event) => {
    event.preventDefault();
    if (this.state.newPassword != this.state.reNewPassword) {
      alert("New password and confirm new password are not equal");
      return;
    }
    try {
      const loggedUser = await Auth.completeNewPassword(
        this.state.user,
        this.state.newPassword
      );
      await this.props.history.push('/dev')
    } catch (error) {
      alert(error.message);
    }
  }



  render() {
    return (

      <div class="bg-dark">
        <div class="sufee-login d-flex align-content-center flex-wrap">
          <div class="container">
            <div class="login-content">
              <div class="login-logo">
                {/* <a href="index.html">
                  <img class="align-content" src="images/logo.png" alt="" />
                </a> */}
              </div>
              <div class="login-form">
                {!this.state.showNewPasswordRequire ? (
                  <form>
                    <div class="form-group">
                      <label>Email address</label>
                      <input type="email" name="username"
                        type="text"
                        className="form-control"
                        placeholder="Email"
                        required=""
                        onChange={this.handleChange}
                        value={this.state.username} />
                    </div>
                    <div class="form-group">
                      <label>Password</label>
                      <input
                        name="password"
                        type="password"
                        className="form-control"
                        placeholder="Password"
                        required={true}
                        onChange={this.handleChange}
                        value={this.state.password}
                      />

                    </div>
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" /> Remember Me
                          </label>
                      <label class="pull-right">
                        <a href={`/forgotPassword?response_type=code&amp;client_id=14c6m7p8g25methi73hmpsjnkb&amp;redirect_uri=${window.location.origin}/dev`}>Forgotten Password?</a>
                      </label>

                    </div>
                    <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30"
                      onClick={this.handleSubmit}
                    >Sign in</button>
                    <div class="social-login-content">
                      <div class="social-button">
                        <button type="button" class="btn social google btn-flat btn-addon mb-3"
                          onClick={this.onSignInWithGoogle}
                        ><i class="ti-google"></i>Sign in with Google</button>
                      </div>
                    </div>
                  </form>) : (

                    <form>

                      <div class="form-group">
                        <label>New Password</label>
                        <input
                          name="newPassword"
                          type="password"
                          className="form-control"
                          placeholder="New Password"
                          required={true}
                          onChange={this.handleChange}
                          value={this.state.newPassword}
                        />
                      </div>


                      <div class="form-group">
                        <label>Confirm</label>
                        <input
                          name="reNewPassword"
                          type="password"
                          className="form-control"
                          placeholder="Confirm New Password"
                          required={true}
                          onChange={this.handleChange}
                          value={this.state.reNewPassword}
                        />
                      </div>
                      <button
                        type="Submit"
                        className="btn btn-primary m-b-30 m-t-30"
                        onClick={this.handleSubmitNewPassword}>
                        Submit</button>
                    </form>
                  )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SignIn;

