import React from 'react'

import Catalog from '../Sidebar/Catalog'
import UserList from '../Sidebar/UserList'
import workspaceContainer from '../../containers/WorkspaceContainer'
import { toggleSidebar } from '../../helpers/sidebarHelper'
import { SubscribeOne } from 'unstated-x';



class Sidebar extends React.Component {

    renderSidebar = (type) => {
        switch (type) {
            case 'catalog':
                return (
                    <Catalog
                        onDragStartCaptureHandle={this.onDragStartCaptureHandle}
                    ></Catalog>
                )
            case 'userlist':
                return (
                    <UserList></UserList>
                )
            default:
                return
        }
    }
    static elementOverlay



    openDialog = (type) => {
        this.setState({ type }, () => {
            document.getElementById('mobile-nav').style.transform = 'translateX(0%)'
        })
    }

    onHandleToggleSidebarHandle = async (sidebarType) => {
        await toggleSidebar()
        await console.log('check toggleeee')

        if (workspaceContainer.state.sidebarType === null) {
            await workspaceContainer.setState({ sidebarType })
        } else {
            await workspaceContainer.setState({ sidebarType: null })
        }
    }


    render() {

        return (
            <React.Fragment>
                <nav class="navbar navbar-expand-sm navbar-default">
                    <div id="main-menu" class="main-menu collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li>
                                <a onClick={() => this.onHandleToggleSidebarHandle('catalog')}>
                                    <i class="menu-icon fa fa-laptop"></i>Catalog</a>
                            </li>
                            <li class="menu-item-has-children dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Icons</a>
                                <ul class="sub-menu children dropdown-menu">
                                    <li>
                                        <i class="menu-icon fa fa-fort-awesome"></i>
                                        <a>Test Something</a>
                                    </li>

                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
                )
            </React.Fragment >

        )
    }
}


export default Sidebar








