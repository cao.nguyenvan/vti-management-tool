import React from 'react'
import Elements from '../../elements/Elements'
import Spinner from '../../elements/UI/Spinner'



class Catalog extends React.Component {


    state = {

    }


    onDragStartCaptureHandle = (e) => {
        const data = e.target.getAttribute('data-type')
        e.dataTransfer.setData("dataResource", data);
    }

    componentWillMount() {
        let state = {}
        Object.keys(Elements).forEach(e => {
            const els = e.split('::');
            if (!state[els[1]]) state[els[1]] = []

            state[els[1]].push({
                type: e,
                name: els[2]
            })

        })
        this.setState({ state }, () => this.forceUpdate())
    }
    render() {

        return (
            this.state.state ? (
                <nav className="navbar navbar-expand-sm navbar-default">
                    <div id="main-menu" className="main-menu collapse navbar-collapse">
                        <ul className="nav navbar-nav">
                            {
                                Object.keys(this.state.state).map(e => (

                                    <li key={e} className="menu-item-has-children dropdown">
                                        <a href="#!" className="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i className="menu-icon fa fa-table"></i>{e}</a>
                                        <ul className="sub-menu children dropdown-menu">
                                            {
                                                this.state.state[e].map(object => (
                                                    <li
                                                        draggable={true}
                                                        key={object.type}
                                                        data-type={object.type}
                                                        onDragStartCapture={(e) => this.onDragStartCaptureHandle(e)}
                                                    >
                                                        <i className="fa fa-table"></i>
                                                        <a href="#!">{object.name}</a>
                                                    </li>
                                                ))
                                            }
                                        </ul>
                                    </li>
                                ))
                            }
                        </ul>
                    </div>
                </nav>
            ) : (<Spinner></Spinner>)
        )
    }
}

export default Catalog