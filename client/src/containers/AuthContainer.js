import { Container} from 'unstated-x'

class AuthContainer extends Container{
    state={
        user:null,
        admin: false
    }
}

const authContainer = new AuthContainer()
export default authContainer