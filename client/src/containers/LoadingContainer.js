import { Container } from 'unstated-x'

class LoadingContainer extends Container{
    state = {
        loading:false
    }
}

const loadingContainer = new LoadingContainer()
export default loadingContainer