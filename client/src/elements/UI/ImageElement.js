import React from 'react'

export default function ImageElement(props){
    return(
        <image id="v-17" xlinkHref={props.src} width="60" height="60" transform="translate(10,0)"></image>
    )
}