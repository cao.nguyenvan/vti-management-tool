import React from 'react'
import { Subscribe } from 'unstated-x'
import { BindingContext } from '../../containers/ElementContainer'
import enhanceElement from '../../helpers/enhanceElement'
import { Maps } from '../../helpers/mapping'

import ImageElement from '../UI/ImageElement'


class EC2SecurityGroup extends React.Component {


    static defaultProps = {
        InstanceType: 't2.micro',
        ImageId: Maps.AWSInstanceType2Arch["t2.micro"].Arch,
        Name: '',
        Description: '',
        SecurityGroupIngress: []
    }

    static ref = React.createRef()
    static generateInspector = () => {
        return (
            <BindingContext.Consumer>
                {dataContainer => (
                    <Subscribe to={[dataContainer]}>
                        {
                            data => (
                                <div>
                                    <div className="form-group">
                                        <label>Name </label>
                                        <input type="text" className="form-control" placeholder="Enter Name ..."
                                            onChange={(e) => data.setState({ Name: e.target.value })}
                                            value={data.state.Name}>
                                        </input>
                                    </div>

                                    <div className="form-group">
                                        <label>GroupDescription </label>
                                        <input type="text" className="form-control" placeholder="Enter Description ..."
                                            onChange={(e) => data.setState({ GroupDescription: e.target.value })}
                                            value={data.state.GroupDescription}>
                                        </input>
                                    </div>

                                    <label>SecurityGroupIngress</label>
                                    <React.Fragment>
                                        {data.state.SecurityGroupIngress.map((e, index) => (
                                            <div className="row" key={index} id="form">
                                                <div className="col col-3">
                                                    <label><strong>{e.IpProtocol}</strong></label>
                                                </div>
                                                <div className="col col-3">
                                                    <label>{e.FromPort}</label>
                                                </div>
                                                <div className="col col-3">
                                                    <label>{e.ToPort}</label>
                                                </div>
                                                <div className="col col-3">
                                                    <label>{e.CidrIp}</label>
                                                </div>
                                            </div>
                                        ))}
                                    </React.Fragment>

                                    <form className="form-group" ref={EC2SecurityGroup.ref}>
                                        <input type="text" className="form-control" name="IpProtocol" placeholder="IpProtocol" defaultValue="tcp" />

                                        <input type="number" className="form-control" name="FromPort" placeholder="FromPort" defaultValue="22" />

                                        <input type="number" className="form-control" name="ToPort" placeholder="ToPort" defaultValue="22" />


                                        <input type="text" className="form-control" name="CidrIp" placeholder="CidrIp" defaultValue="0.0.0.0/0" />


                                        <button className="btn fas fa-plus-circle"
                                            onClick={e => {
                                                const elements = EC2SecurityGroup.ref.current.elements
                                                let securityGroupIngress = data.state.SecurityGroupIngress
                                                let object = {}
                                                for (let i = 0; i < elements.length - 1; i++) {
                                                    object[elements[i].name] = elements[i].value
                                                }
                                                securityGroupIngress.push(object)
                                                data.setState({ SecurityGroupIngress: securityGroupIngress })
                                                e.preventDefault()
                                            }}
                                            style={{ cursor: 'pointer', float: 'left' }}></button>


                                    </form>




                                </div>
                            )
                        }
                    </Subscribe>
                )}
            </BindingContext.Consumer>)
    }

    render() {
        return (
            <ImageElement src="https://d1ys8zxr0l1qjo.cloudfront.net/img/fa3cd6cf33f5331bf40a9ae8d783b7ddabbbff38/resource-icons/ec2-securitygroup.svg" />
        )
    }
}

export default enhanceElement(EC2SecurityGroup) 