import React from 'react'
import enhanceElement from '../../helpers/enhanceElement'
import { Maps } from '../../helpers/mapping'

import ImageElement from '../UI/ImageElement'

import BindSelectInput from '../../components/Inspector/Binding/BindSelectInput'
import BindTag from '../../components/Inspector/Binding/BindTag'
import BindTextInput from '../../components/Inspector/Binding/BindTextInput'

// Object.keys(Maps.AWSInstanceType2Arch).map(e => (
//     <option key={e} value={e}>{e}</option>
// ))
class EC2Instance extends React.Component {


    static defaultProps = {
        InstanceType: 't2.micro',
        ImageId: Maps.AWSInstanceType2Arch["t2.micro"].Arch,
        Name: '',
        Description: '',
        Tags: []
    }

    static ref = React.createRef()
    static generateInspector = () => {
        return (
            <React.Fragment>
                <BindTextInput title="Name"
                    bind="data.Name"
                    placeholder="name..."
                    type="textinput"
                ></BindTextInput>


                <BindTextInput title="Description"
                    bind="data.Description"
                    placeholder="description..."
                    type="textarea"
                ></BindTextInput>

                <BindSelectInput
                    title="Instance Type"
                    placeholder="Choose your Type"
                    bind="data.InstanceType"
                    listItem={Object.keys(Maps.AWSInstanceType2Arch)}
                />

                <BindTag />

            </React.Fragment>
        )
    }

    render() {
        return (
            <ImageElement src="https://d1ys8zxr0l1qjo.cloudfront.net/img/fa3cd6cf33f5331bf40a9ae8d783b7ddabbbff38/resource-icons/ec2-instance.svg">
            </ImageElement>
        )
    }
}

export default enhanceElement(EC2Instance) 