import React from 'react'
import { Subscribe } from 'unstated-x'
import { BindingContext } from '../../containers/ElementContainer'
import enhanceElement from '../../helpers/enhanceElement'

import ImageElement from '../UI/ImageElement'


class IAMInstanceProfile extends React.Component {


    static defaultProps = {
        Name: '',
        Roles: []
    }

    static ref = React.createRef()
    static generateInspector = () => {
        return (
            <BindingContext.Consumer>
                {dataContainer => (
                    <Subscribe to={[dataContainer]}>
                        {
                            data => (
                                <div>
                                    <div className="form-group">
                                        <label>Name </label>
                                        <input type="text" className="form-control" placeholder="Enter Name ..."
                                            onChange={(e) => data.setState({ Name: e.target.value })}
                                            value={data.state.Name}>
                                        </input>
                                    </div>

                                    <div className="form-group">
                                        <label>Roles </label>
                                        <input type="text" className="form-control" placeholder="Enter Role ..."
                                            onChange={(e) => data.setState({ Roles: [e.target.value] })}
                                            value={data.state.Roles[0]}>
                                        </input>
                                    </div>
                                </div>
                            )
                        }
                    </Subscribe>
                )}
            </BindingContext.Consumer>)
    }

    render() {
        return (
            <ImageElement src="https://d1ys8zxr0l1qjo.cloudfront.net/img/fa3cd6cf33f5331bf40a9ae8d783b7ddabbbff38/resource-icons/rds-dbinstance.svg" />
        )
    }
}

export default enhanceElement(IAMInstanceProfile) 