import React from 'react'
import { Subscribe } from 'unstated-x'
import { BindingContext } from '../../containers/ElementContainer'
import enhanceElement from '../../helpers/enhanceElement'
import { Maps } from '../../helpers/mapping'

import ImageElement from '../UI/ImageElement'

class RDSSecurityGroup extends React.Component {


    static defaultProps = {
        InstanceType: 't2.micro',
        ImageId: Maps.AWSInstanceType2Arch["t2.micro"].Arch,
        Name: '',
        Description: '',
        DBSecurityGroupIngress: []
    }

    static ref = React.createRef()
    static generateInspector = () => {
        return (
            <BindingContext.Consumer>
                {dataContainer => (
                    <Subscribe to={[dataContainer]}>
                        {
                            data => (
                                <div>
                                    <div className="form-group">
                                        <label>Name </label>
                                        <input type="text" className="form-control" placeholder="Enter Name ..."
                                            onChange={(e) => data.setState({ Name: e.target.value })}
                                            value={data.state.Name}>
                                        </input>
                                    </div>

                                    <div className="form-group">
                                        <label>GroupDescription </label>
                                        <input type="text" className="form-control" placeholder="Enter Description ..."
                                            onChange={(e) => data.setState({ GroupDescription: e.target.value })}
                                            value={data.state.GroupDescription}>
                                        </input>
                                    </div>

                                    <label>SecurityGroupIngress</label>
                                    <React.Fragment>
                                        {data.state.DBSecurityGroupIngress.map((e, index) => (
                                            <div className="row" key={index} id="form">
                                                <div className="col col-3">
                                                    <label>{e.CIDRIP}</label>
                                                </div>
                                            </div>
                                        ))}
                                    </React.Fragment>

                                    <form className="form-group" ref={RDSSecurityGroup.ref}>
                                        <input type="text" className="form-control" name="CIDRIP" placeholder="CIDRIP" defaultValue="0.0.0.0/0" />

                                        <button className="btn fas fa-plus-circle"
                                            onClick={e => {
                                
                                                const elements = RDSSecurityGroup.ref.current.elements
                                                let DBSecurityGroupIngress = data.state.DBSecurityGroupIngress
                                                let object = {}
                                                for (let i = 0; i < elements.length - 1; i++) {
                                                    object[elements[i].name] = elements[i].value
                                                }
                                                DBSecurityGroupIngress.push(object)
                                                data.setState({ DBSecurityGroupIngress })
                                                e.preventDefault()
                                            }}
                                            style={{ cursor: 'pointer', float: 'left' }}></button>


                                    </form>




                                </div>
                            )
                        }
                    </Subscribe>
                )}
            </BindingContext.Consumer>)
    }

    render() {
        return (
            <ImageElement src="https://d1ys8zxr0l1qjo.cloudfront.net/img/fa3cd6cf33f5331bf40a9ae8d783b7ddabbbff38/resource-icons/rds-dbinstance.svg"></ImageElement>       
        )
    }
}

export default enhanceElement(RDSSecurityGroup) 