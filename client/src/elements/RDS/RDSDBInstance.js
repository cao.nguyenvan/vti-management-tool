import React from 'react'
import { Subscribe } from 'unstated-x'
import { BindingContext } from '../../containers/ElementContainer'
import enhanceElement from '../../helpers/enhanceElement'
import { Maps,RDS_CLASSES } from '../../helpers/mapping'
import ImageElement from '../UI/ImageElement';


class RDSDBInstance extends React.Component {


    static defaultProps = {
        Engine: "MySQL",
        EngineVersion: "5.6.13",
        Name: '',
        Tags: [],
        DBInstanceClass:'db.t2.small'
    }

    static ref = React.createRef()
    static generateInspector = () => {
        return (
            <BindingContext.Consumer>
                {dataContainer => (
                    <Subscribe to={[dataContainer]}>
                        {
                            data => (
                                <div>
                                    <div className="form-group">
                                        <label>DBName </label>
                                        <input type="text" className="form-control" placeholder="Enter Name ..."
                                            onChange={(e) => data.setState({ Name: e.target.value })}
                                            value={data.state.Name}>
                                        </input>
                                    </div>

                                    <div className="form-group">
                                        <label>Engine</label>
                                        <select value={data.state.Engine} className="form-control"
                                        onChange={(e) => data.setState({Engine:e.target.value})}
                                        >
                                           <option value="MySQL">MySQL</option>
                                           <option value="postgres">PostgreSQL</option>
                                         </select>
                                    </div>
                                    <div className="form-group">
                                    <label>DBInstanceClass</label>
                                      <select className="form-control" onChange={(e)=> data.setState({DBInstanceClass:e.target.value})}
                                      value = {data.state.DBInstanceClass}>
                                         {
                                             RDS_CLASSES.map(e=>(
                                                 <option key={e} value={e}>{e}</option>
                                             ))
                                         }
                                      </select>
                                    </div>
                                    <label>Tags</label>
                                    <React.Fragment>
                                        {
                                            data.state.Tags.map(e => (
                                                <div className="row" key={e.Key} id="form">
                                                    <div className="col col-4">
                                                        <label><strong>{e.Key}</strong></label>
                                                    </div>
                                                    <div className="col col-4">
                                                        <label>{e.Value}</label>
                                                    </div>
                                                </div>
                                            ))

                                        }
                                    </React.Fragment>

                                    <form className="row" ref={RDSDBInstance.ref}>

                                        <div className="col col-5">
                                            <input type="text" className="form-control" placeholder="Key" />
                                        </div>
                                        <div className="col col-5">
                                            <input type="text" className="form-control" placeholder="Value" />
                                        </div>
                                        <div className="col col-2">
                                            <button className="btn fas fa-plus-circle fa-x2"
                                                onClick={e => {
                                                    const key = RDSDBInstance.ref.current.elements[0].value
                                                    const value = RDSDBInstance.ref.current.elements[1].value
                                                    let tags = data.state.Tags
                                                    tags.push({ Key: key, Value: value })
                                                    data.setState({ Tags: tags })
                                                    e.preventDefault()
                                                }}
                                                style={{ cursor: 'pointer', float: 'left' }}></button>
                                        </div>

                                    </form>




                                </div>
                            )
                        }
                    </Subscribe>
                )}
            </BindingContext.Consumer>)
    }

    render() {
        return (
            <ImageElement src="https://d1ys8zxr0l1qjo.cloudfront.net/img/fa3cd6cf33f5331bf40a9ae8d783b7ddabbbff38/resource-icons/rds-dbinstance.svg"></ImageElement>       
        )
    }
}

export default enhanceElement(RDSDBInstance) 