import axios from 'axios'
import loadingContainer from '../containers/LoadingContainer'
class AxiosX {
    constructor(){
        this.path = window.location.href.indexOf('/dev') !== -1 || window.location.hostname ==='localhost'? '/dev' : ''
        
    }

    get = async (url , spinner= true)=>{
        if(spinner){
            await loadingContainer.setState({loading:true})
            const result = await axios.get(`${this.path}${url}`)
            await loadingContainer.setState({loading:false})
            return result
        }else{
            return await axios.get(`${this.path}${url}`)
        }
        
    }

    post = async (url, params , spinner = true)=>{
        return await axios.post(`${this.path}${url}`, params)
    }

}

const axiosx = new AxiosX()

export default axiosx