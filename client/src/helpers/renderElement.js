import { SubscribeOne } from 'unstated-x'
import React from 'react'
import Elements from '../elements/Elements'
import ElementContainer from '../containers/ElementContainer'
export default function renderElement(arrayOfElement) {

    return (
        <React.Fragment>
            {arrayOfElement.map((e) => {
                window.e = e
                const ElementComponent = Elements[e[1].state.type]
                const element = ElementContainer.get(e[0])
                return (<SubscribeOne key={e[0]} to={e[1]} bind={['x', 'y','']}>
                    {
                        elementContainer => (
                            <SubscribeOne to={e[1].data} bind={['Name']}>
                                {
                                    elementData=>(
                                        <svg  x={elementContainer.state.x} y={elementContainer.state.y}>
                                        <g data-element={elementContainer.state.id}
                                        ref={element.domNodeRef}
                                        draggable={true}
                                        style={{ cursor: 'all-scroll' }}
                                        >
                                           <ElementComponent></ElementComponent>
                                            <text className="icon-sup-text" id="v-20" fontSize="9px" 
                                            fontFamily="'helvetica neue', roboto, arial, 'droid sans', sans-serif" 
                                            fill="#666666" pointerEvents="none" textAnchor="middle" y="0.8em" display="null" 
                                            xmlSpace="preserve" transform="translate(40,60)">
                                                <tspan dy="0em" x="0" id="v-35" className="v-line">{e[1].state.type.split('::')[2]}</tspan>
                                            </text>
                                            <text className="icon-text" id="v-21" fontSize="10px" 
                                            fontFamily="'helvetica neue', roboto, arial, 'droid sans', sans-serif" 
                                            fontWeight="bold" fill="black" pointerEvents="none" textAnchor="middle" y="0.8em" display="null" 
                                            xmlSpace="preserve" transform="translate(40,48)">
                                                <tspan dy="0em" x="0" id="v-34" className="v-line">{elementData.state.Name}</tspan>
                                            </text>
                                        </g >
                                    </svg>
                                    )
                                }
                        </SubscribeOne>)}
                           
                </SubscribeOne>)
            })}
        </React.Fragment>
    )
}