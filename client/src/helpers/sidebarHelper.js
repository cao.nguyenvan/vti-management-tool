export  function toggleSidebar() {
    var windowWidth = window.$(window).width();
    if (windowWidth < 1010) {
        window.$('body').removeClass('open');
        if (windowWidth < 760) {
            window.$('#left-panel').slideToggle();
        } else {
            window.$('#left-panel').toggleClass('open-menu');
        }
    } else {
        window.$('body').toggleClass('open');
        window.$('#left-panel').removeClass('open-menu');
    }
}