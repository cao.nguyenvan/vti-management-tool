
import axiosx from './axiosx'
import { REDIRECT_SIGNIN } from '../config'
import { APP_CLIENT_ID, POOL_ID } from '../initFromServer'

var Netmask = require('netmask').Netmask
window.netmask = Netmask

export function readJsonFileAsText(file) {
	return new Promise(resolve => {
		const reader = new FileReader()
		reader.onload = (e) => {

			window.target = e.target
			resolve(JSON.parse(e.target.result))
		}
		file && reader.readAsText(file)
	})
}



export function downloadObjectAsJson(exportName, template) {
	const dataStr = 'data:text/plain;charset=utf-8,' + encodeURIComponent(template)
	const downloadAnchorNode = document.createElement('a')
	downloadAnchorNode.setAttribute('href', dataStr)
	downloadAnchorNode.setAttribute('download', exportName + '.pem')
	const e = document.createEvent('MouseEvents')
	e.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null)
	downloadAnchorNode.dispatchEvent(e)
	downloadAnchorNode.remove()
}

export async function checkAdmin(cognitoUser) {

	const res = await axiosx.get(`/apis/user/checkadmin?userpoolid=${POOL_ID}&username=${cognitoUser.username}`)
	return res.data
}


export async function addCallbackUrlToAppClient() {
	const res = await axiosx.post(`/apis/sso/addurlclient`, {
		clientId: APP_CLIENT_ID,
		poolUserID: POOL_ID,
		callbackURL: REDIRECT_SIGNIN
	})
	return res
}



export function findCidrBlocks(subnetCidrs, number) {
	const lastips = subnetCidrs.map(e => {
		const block = new Netmask(e)
		return block.broadcast
	})
	const findMaxIp = (lastips) => {


		const compareIp = (a, b) => {
			const ipa = a.split('.').map(e => Number(e))
			const ipb = b.split('.').map(e => Number(e))
			for (let i = 0; i < ipa.length; i++) {
				if (ipa[i] > ipb[i]) {
					return false
				} else if (ipa[i] < ipb[i]) {
					return true
				}
			}
		}
		const plusIp = (ip) => {
			let ipelements = ip.split('.').map(e => Number(e))
			let i = ipelements.length - 1
			while (true) {
				if (ipelements[i] == 255) {
					ipelements[i] = 0
					i--;
				} else if (i == 0) {
					return null
				} else {
					ipelements[i] = ipelements[i] + 1
					return ipelements.map(e => e.toString()).join('.')
				}
			}
		}

		window.plusIp = plusIp
		let maxip = lastips[0]
		for (let i = 0; i < lastips.length; i++) {
			if (compareIp(maxip, lastips[i])) {
				maxip = lastips[i]
			}
		}

		return plusIp(maxip)
	}

	const calculateRangeIps = (number) => {
		let i = 4
		while (true) {
			if (Math.pow(2, i) >= number) {
				return i
			} else {
				i++;
			}
		}
	}

	const maxIp = findMaxIp(lastips)
	const rangeBlock = 32 - calculateRangeIps(number + 6)
	return `${maxIp}/${rangeBlock}`
}